module.exports = {
  root: true,
  env: {
    node: true,
  },
  extends: ["plugin:vue/essential", "eslint:recommended", "@vue/prettier"],
  parserOptions: {
    parser: "babel-eslint",
  },
  rules: {
    "no-console": "off",
    "no-debugger": "off",
    "prettier/prettier": "error",
    "no-unused-vars": "off",
    "vue/no-unused-vars": "off",
    "vue/no-unused-components": "off",
    "vue/no-use-v-if-with-v-for": "off",
    "vue/require-v-for-key": "off",
    "no-undef": "off",
  },
  overrides: [
    {
      files: [
        "**/__tests__/*.{j,t}s?(x)",
        "**/tests/unit/**/*.spec.{j,t}s?(x)",
      ],
      env: {
        mocha: true,
      },
    },
  ],
};
