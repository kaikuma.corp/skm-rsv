import {
  newRequest,
  extractData,
  validateResponse,
  getToken,
  extractDataContent
} from "./v1";
import url from "url";
import qs from "querystring";
import { env } from "@/utils";

/**
 * @function
 * @decription 外帶的分店明細
 * @param {string} obj.qlieercname
 */
export const getTakeOutMenu = ({ qlieercname }) => {
  return newRequest({
    method: "GET",
    url: env.VUE_APP_SKM_API_NEW_PATH,
    params: {
      value: JSON.stringify({
        ProgApiContent: "RestaurantTakeOut",
        TokenContent: getToken(),
        DataContent: {
          qlieercname
        }
      })
    }
  }).then(extractDataContent);
};

export const createOrder = (
  data,
  ProgApiContent,
  receiveCartId,
  query = {}
) => {
  return newRequest({
    method: "POST",
    url: "/vc/v/",
    // withCredentials: true,
    // params: {
    //   value: JSON.stringify({
    //     ProgApiContent: "CartPayEncry",
    //     TokenContent: getToken(),
    //     DataContent: data
    //   })
    // }
    data: {
      ProgApiContent: ProgApiContent,
      TokenContent: getToken(),
      DataContent: data
    }
  })
    .then(extractDataContent)
    .then(res => {
      // console.log("error:");
      // console.log(res);
      if (res.unavailableProducts) {
        const err = new Error("UnavailableProducts");
        err.unavailableProducts = res.unavailableProducts;
        throw err;
      }
      receiveCartId(res.cartId);
      return res;
    })
    .then(res => {
      if (ProgApiContent === "CartPayEncry") {
        // console.log("createOrder:");
        // console.log(res);
        const currUrl = url.parse(location.href);
        const query = qs.parse(currUrl.query);
        query.cart_id = res.cartId;
        currUrl.search = "?" + qs.stringify(query);

        const form = document.createElement("form");
        let actionUrl =
          env.VUE_APP_PAYMENT_ENDPOINT +
          "/website/api/api.receive_order_skmfrs.php";
        if (query.bdmpid) {
          actionUrl += "?bdmpid=" + query.bdmpid;
        }
        form.setAttribute("method", "post");
        form.setAttribute("action", actionUrl);
        form.style.width = "0px";
        form.style.height = "0px";
        form.style.opacity = "0";

        const dataInput = document.createElement("input");
        dataInput.setAttribute("type", "text");
        dataInput.setAttribute("name", "data");
        dataInput.value = res.data;
        form.appendChild(dataInput);

        const checksumInput = document.createElement("input");
        checksumInput.setAttribute("type", "text");
        checksumInput.setAttribute("name", "checksum");
        checksumInput.value = res.checksum;
        form.appendChild(checksumInput);

        const urlInput = document.createElement("input");
        urlInput.setAttribute("type", "text");
        urlInput.setAttribute("name", "previous_url");
        urlInput.value = url.format(currUrl);
        form.appendChild(urlInput);

        document.body.appendChild(form);
        form.submit();
        setTimeout(() => {
          document.body.removeChild(form);
        });
      } else if (ProgApiContent === "CartSkmPayEncry") {
        return res;
      }
    });
};

export const getOrder = (orderId, memberId) => {
  return newRequest({
    method: "GET",
    url: env.VUE_APP_SKM_API_NEW_PATH,
    params: {
      value: JSON.stringify({
        ProgApiContent: "ViewOrderResult",
        TokenContent: getToken(),
        DataContent: {
          OrderID: orderId,
          MemberId: memberId
        }
      })
    }
  }).then(extractDataContent);
};

export const checkOrderStatus = (orderId, memberId) => {
  return newRequest({
    method: "GET",
    url: env.VUE_APP_SKM_API_NEW_PATH,
    params: {
      value: JSON.stringify({
        ProgApiContent: "OrderCheckTakeOut",
        TokenContent: getToken(),
        DataContent: {
          OrderId: orderId,
          MemberId: memberId
        }
      })
    }
  }).then(extractDataContent);
};

export const contact = (orderId, email, content, id, name) => {
  return newRequest({
    method: "POST",
    url: "/vc/v/",
    data: {
      ProgApiContent: "SaveOrderCusServices",
      TokenContent: getToken(),
      DataContent: {
        OrderId: orderId,
        MemberName: name,
        MemberEmail: email,
        MemberId: id,
        EmailContent: content
      }
    }
  }).then(extractDataContent);
};

export const deleteOrder = (orderId, account) =>
  newRequest({
    method: "GET",
    url: `/vc/v/`,
    withCredentials: true,
    params: {
      value: JSON.stringify({
        ProgApiContent: "OrderBackEndCancel",
        TokenContent: "",
        DataContent: {
          MemberId: account.id,
          MemberName: account.name,
          OrderId: orderId
        }
      })
    }
  });

export const createSkmpay = (data, query = {}) => {
  return newRequest({
    method: "POST",
    url: "/vc/v/",
    // withCredentials: true,
    // params: {
    //   value: JSON.stringify({
    //     ProgApiContent: "CartPayEncry",
    //     TokenContent: getToken(),
    //     DataContent: data
    //   })
    // }
    data: {
      ProgApiContent: "CartSkmPayConnect",
      TokenContent: getToken(),
      DataContent: data
    }
  })
    .then(extractDataContent)
    .then(res => {
      return res;
    });
};

export const checkSkmpayCartResult = mOrderUUID =>
  newRequest({
    method: "POST",
    url: "/vc/v/",
    data: {
      ProgApiContent: "CartCheckSkmResult",
      TokenContent: getToken(),
      DataContent: {
        mOrderUUID
      }
    }
  }).then(extractDataContent);

export const getSkmpayCartResult = mOrderUUID =>
  newRequest({
    method: "POST",
    url: "/vc/v/",
    data: {
      ProgApiContent: "CartViewPayResult",
      TokenContent: getToken(),
      DataContent: {
        mOrderUUID
      }
    }
  }).then(extractDataContent);
