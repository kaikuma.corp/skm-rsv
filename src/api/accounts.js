import { newRequest, extractData, extractDataContent, getToken } from "./v1";
import { env } from "@/utils";

export const getiFrameLoginUri = () => {
  return `${env.VUE_APP_SKM_API_NEW_ENDPOINT}/LoginPath/mloginPages`;
};

export const status = () =>
  newRequest
    .get("/accounts/status", { withCredentials: true })
    .then(extractData)
    .catch(() => ({ login: false }));

export const logout = () =>
  newRequest("/LoginPath/logout", {
    method: "post",
    withCredentials: true
  });

export const getAccount = () =>
  newRequest.get("/LoginPath/me", { withCredentials: true }).then(extractData);

/**
 * @function
 * @description 訂/候位紀錄列表
 * @param {string} memberId
 */
export const getRecords = memberId => {
  return newRequest({
    method: "GET",
    url: env.VUE_APP_SKM_API_NEW_PATH,
    params: {
      value: JSON.stringify({
        ProgApiContent: "OrderHistoryListWaiting",
        TokenContent: getToken(),
        DataContent: {
          MemberId: memberId
        }
      })
    }
  }).then(extractDataContent);
};

/**
 * @function
 * @description 外帶/外送位紀錄列表
 * @param {string} memberId
 */
export const getOrders = memberId => {
  return newRequest({
    method: "GET",
    url: env.VUE_APP_SKM_API_NEW_PATH,
    params: {
      value: JSON.stringify({
        ProgApiContent: "OrderHistoryListTakeOut",
        TokenContent: getToken(),
        DataContent: {
          MemberId: memberId
        }
      })
    }
  }).then(extractDataContent);
};

export const getLoginUri = passcode => {
  return `http://uessay.skm.com.tw:8600/OAuth.aspx${passcode}`;
};

/**
 * @function
 * @decription 獲取登入參數
 */
export const getMemberParameter = () => {
  return newRequest({
    method: "GET",
    url: `/vc/v/`,
    params: {
      value: JSON.stringify({
        ProgApiContent: "GetMemberLoginParameter",
        TokenContent: getToken(),
        DataContent: ""
      })
    }
  }).then(extractDataContent);
};
