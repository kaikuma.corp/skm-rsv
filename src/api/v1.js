import axios from "axios";
import i18n from "@/i18n";
import { env } from "@/utils";
import Cookies from "js-cookie";

export const newRequest = axios.create({
  baseURL: env.VUE_APP_SKM_API_NEW_ENDPOINT,
  validateStatus: status => {
    if (status === 401) {
      alert(i18n.t("errorCodes.skm-N1"));
      window.location = "/";
      return false;
    }
    return true;
  }
});

export const getToken = () => {
  return Cookies.get("TokenContent");
};

export const validateResponse = res => {
  if (res.status == 503 && res.data.logout) {
    alert(i18n.t(`errorCodes.${res.data.code}`));
    location.href = "/";
    return;
  }
  if (res.status >= 400) {
    const err = new Error(res.data.error);
    err.status = res.status;
    err.code = res.data.code;
    err.response = res;
    throw err;
  }
  return res;
};

export const extractData = res => {
  if (res.status == 503 && res.data.logout) {
    alert(i18n.t(`errorCodes.${res.data.code}`));
    location.href = "/";
    return;
  }
  if (res.status >= 400) {
    const err = new Error(res.data.error);
    err.status = res.status;
    err.code = res.data.code;
    err.response = res;
    throw err;
  }
  return res.data.data;
};

export const extractDataContent = res => {
  // console.log("extractDataContent:");
  // console.log(res.data);
  if (res.data === null) return;
  const { TokenContent } = res.data;

  if (res.status == 503 && res.data.logout) {
    alert(i18n.t(`errorCodes.${res.data.code}`));
    location.href = "/";
    return;
  }
  if (res.status >= 400) {
    const err = new Error(res.data.error);
    err.status = res.status;
    err.code = res.data.code;
    err.response = res;
    throw err;
  }

  if (TokenContent) {
    Cookies.set("TokenContent", TokenContent);
  }

  return res.data.DataContent;
};
