// import instance from "./instance";
import _ from "underscore";
import moment from "moment";
import { newRequest, getToken, extractDataContent } from "./v1";
import { env } from "@/utils";

// export const find = (id, params = {}) => {
//   params = _.defaults(params, {
//     type: "all",
//     size: params.numOfPersons,
//     date: moment().format("YYYY-MM-DD")
//   });

//   return instance.get(`v2/groups/${id}`, { params });
// };

/**
 * @function
 * @description 取得分店資料
 * @param {string} obj.groupId
 */
export const get = ({ groupId }) => {
  return newRequest({
    method: "GET",
    url: env.VUE_APP_SKM_API_NEW_PATH,
    params: {
      value: JSON.stringify({
        ProgApiContent: "Group",
        TokenContent: getToken(),
        DataContent: {
          GroupId: groupId
        }
      })
    }
  }).then(extractDataContent);
};
