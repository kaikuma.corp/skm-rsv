import * as Group from "./group";
import * as Branch from "./branch";
import * as Reservation from "./reservation";
import * as Landing from "./landing";
import * as Order from "./order";
import * as Accounts from "./accounts";
import * as Restaurant from "./restaurant";
import { env } from "@/utils";

const endpoint = env.VUE_APP_SKM_API_NEW_ENDPOINT;

export {
  Group,
  Branch,
  Reservation,
  Landing,
  Order,
  Accounts,
  Restaurant,
  endpoint
};
