// import instance from "./instance";
import moment from "moment";
import { newRequest, getToken, extractDataContent } from "./v1";
import { env } from "@/utils";

// export const find = (groupId, companyId, branchId, params = {}) => {
//   params.type = "all";
//   params.date = params.date || moment().format("YYYY-MM-DD");
//   params.size = params.size || 2;

//   return instance.get(
//     `v2/groups/${groupId}/companies/${companyId}/branches/${branchId}`,
//     { params }
//   );
// };

/**
 * @function
 * @description 取得縣市、區域
 */
export const getAreas = () => {
  return newRequest({
    method: "GET",
    url: env.VUE_APP_SKM_API_NEW_PATH,
    params: {
      value: JSON.stringify({
        ProgApiContent: "Area",
        TokenContent: getToken(),
        DataContent: ""
      })
    }
  }).then(extractDataContent);
};

/**
 * @function
 * @description 取得所有分館
 */
export const getBranches = () => {
  return newRequest({
    method: "GET",
    url: env.VUE_APP_SKM_API_NEW_PATH,
    params: {
      value: JSON.stringify({
        ProgApiContent: "Branches",
        TokenContent: getToken(),
        DataContent: {}
      })
    }
  }).then(extractDataContent);
};

/**
 * @function
 * @decription 查詢分館
 * @param {string} obj.City
 * @param {string} obj.Area
 * @param {string} obj.Address
 */
export const searchBranch = ({ City, Area, Address }) => {
  return newRequest({
    method: "GET",
    url: env.VUE_APP_SKM_API_NEW_PATH,
    params: {
      value: JSON.stringify({
        ProgApiContent: "SearchBranch",
        TokenContent: getToken(),
        DataContent: {
          City,
          Area,
          Address
        }
      })
    }
  }).then(extractDataContent);
};
