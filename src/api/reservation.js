// import instance from "./instance";
import _ from "underscore";
import {
  newRequest,
  extractData,
  getToken,
  extractDataContent,
  validateResponse
} from "./v1";
import { env } from "@/utils";

// export const create = (restaurant, reservation = {}) => {
//   const company = restaurant.company;

//   reservation = _.defaults(reservation, {
//     createdFrom: "web",
//     fbPage: "",
//     fbUser: ""
//   });

//   return instance.post(
//     `reservations/${company.id}/${restaurant.id}`,
//     reservation
//   );
// };

export const createWaiting = data => {
  return newRequest({
    method: "POST",
    url: env.VUE_APP_SKM_API_NEW_PATH,
    // params: {
    //   value: JSON.stringify({
    //     ProgApiContent: "WaitingPosition",
    //     TokenContent: getToken(),
    //     DataContent: data
    //   })
    // }
    data: {
      ProgApiContent: "WaitingPosition",
      TokenContent: getToken(),
      DataContent: data
    }
  }).then(extractDataContent);
};

export const createBooking = (restaurant, data, account) => {
  data.companyId = restaurant.company.id;
  data.branchId = restaurant.id;
  return newRequest({
    method: "POST",
    url: env.VUE_APP_SKM_API_NEW_PATH,
    // params: {
    //   value: JSON.stringify({
    //     ProgApiContent: "BookingPosition",
    //     TokenContent: getToken(),
    //     DataContent: { ...data, ...account }
    //   })
    // }
    data: {
      ProgApiContent: "BookingPosition",
      TokenContent: getToken(),
      DataContent: { ...data, ...account }
    }
  }).then(extractDataContent);
};
