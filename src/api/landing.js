const branches = [
  {
    GroupId: "skm-TaipeiStation",
    Title: "台北站前店",
    Lat: 25.04624,
    Lng: 121.513104
  },
  {
    GroupId: "skm-TaipeiTianmu",
    Title: "台北天母店",
    Lat: 25.1179863,
    Lng: 121.5313583
  },

  {
    GroupId: "skm-xinyi",
    Title: "台北信義新天地",
    Lat: 25.0366128,
    Lng: 121.565063
  },
  {
    GroupId: "skm-nanxi",
    Title: "台北南西店",
    Lat: 25.052264,
    Lng: 121.521094
  },
  // {
  //   GroupId: 'nanxifoodmap',
  //   Title: '南西商圈',
  //   Lat: 25.052264,
  //   Lng: 121.521094,
  // },
  // {
  //   GroupId: "skm-TaipeiStation",
  //   Title: "台北站前店",
  //   Lat: 25.04624,
  //   Lng: 121.513104
  // },
  // {
  //   GroupId: "skm-TaipeiTianmu",
  //   Title: "台北天母店",
  //   Lat: 25.1179863,
  //   Lng: 121.5313583
  // },
  {
    GroupId: "skm-TaoyuanDayou",
    Title: "桃園大有店",
    Lat: 25.008079,
    Lng: 121.320073
  },
  {
    GroupId: "skm-TaoyuanStation",
    Title: "桃園站前店",
    Lat: 24.989979,
    Lng: 121.312637
  },
  {
    GroupId: "skm-taichung",
    Title: "台中中港店",
    Lat: 24.1652619,
    Lng: 120.641514
  },
  {
    GroupId: "skm-ChiayiChuiyang",
    Title: "嘉義垂楊店",
    Lat: 23.473381,
    Lng: 120.441102
  },
  {
    GroupId: "skm-tainanplace",
    Title: "台南新天地",
    Lat: 22.986775,
    Lng: 120.1987
  },
  {
    GroupId: "skm-TainanZhongshan",
    Title: "台南中山店",
    Lat: 22.995213,
    Lng: 120.209839
  },
  {
    GroupId: "skm-KaohsiungZuoying",
    Title: "高雄左營店",
    Lat: 22.689018,
    Lng: 120.310364
  },
  {
    GroupId: "skm-KaohsiungSanduo",
    Title: "高雄三多店",
    Lat: 22.614298,
    Lng: 120.306738
  }
];

const searchBranch = [
  // {
  //   GroupId: "skm-xinyi",
  //   Title: "台北信義新天地",
  //   Lat: 25.0366128,
  //   Lng: 121.565063
  // },
  // {
  //   GroupId: "skm-TaipeiStation",
  //   Title: "台北站前店",
  //   Lat: 25.04624,
  //   Lng: 121.513104
  // }
];

const area = [
  {
    CityName: "台北市",
    CityArea: [
      {
        AreaID: "001",
        AreaName: "中正區"
      },
      {
        AreaID: "002",
        AreaName: "北投區"
      },
      {
        AreaID: "003",
        AreaName: "信義區"
      },
      {
        AreaID: "004",
        AreaName: "大安區"
      },
      {
        AreaID: "005",
        AreaName: "大同區"
      }
    ]
  },
  {
    CityName: "新北市",
    CityArea: [
      {
        AreaID: "001",
        AreaName: "五股區"
      },
      {
        AreaID: "002",
        AreaName: "蘆洲區"
      },
      {
        AreaID: "003",
        AreaName: "淡水區"
      },
      {
        AreaID: "004",
        AreaName: "汐止區"
      },
      {
        AreaID: "005",
        AreaName: "新店區"
      }
    ]
  },
  {
    CityName: "桃園市",
    CityArea: [
      {
        AreaID: "001",
        AreaName: "五股區"
      },
      {
        AreaID: "002",
        AreaName: "蘆洲區"
      },
      {
        AreaID: "003",
        AreaName: "淡水區"
      },
      {
        AreaID: "004",
        AreaName: "汐止區"
      },
      {
        AreaID: "005",
        AreaName: "新店區"
      }
    ]
  },
  {
    CityName: "新竹市",
    CityArea: [
      {
        AreaID: "001",
        AreaName: "五股區"
      },
      {
        AreaID: "002",
        AreaName: "蘆洲區"
      },
      {
        AreaID: "003",
        AreaName: "淡水區"
      },
      {
        AreaID: "004",
        AreaName: "汐止區"
      },
      {
        AreaID: "005",
        AreaName: "新店區"
      }
    ]
  },
  {
    CityName: "台南市",
    CityArea: [
      {
        AreaID: "001",
        AreaName: "東區"
      },
      {
        AreaID: "002",
        AreaName: "安平區"
      },
      {
        AreaID: "003",
        AreaName: "新化區"
      },
      {
        AreaID: "004",
        AreaName: "西區"
      },
      {
        AreaID: "005",
        AreaName: "xx區"
      }
    ]
  }
];

const options = { branches, area, searchBranch };

const get = name =>
  new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve(options[name]);
    }, 300);
  });

export default {
  get
};
