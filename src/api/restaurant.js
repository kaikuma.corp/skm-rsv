import moment from "moment";
import { env } from "@/utils";
import { newRequest, getToken, extractDataContent } from "./v1";

/**
 * @function
 * @decription 查詢餐廳
 * @param {string} obj.groupId
 * @param {string} obj.companyId
 * @param {string} obj.restaurantId
 */
// TODO： 缺少bookingInfo內容
export const get = ({ groupId, companyId, restaurantId }) => {
  return newRequest({
    method: "GET",
    url: env.VUE_APP_SKM_API_NEW_PATH,
    params: {
      value: JSON.stringify({
        ProgApiContent: "Restaurant",
        TokenContent: getToken(),
        DataContent: {
          groupId,
          RestaurantId: restaurantId,
          CompanyId: companyId,
          SearchDateTime: moment().format("YYYY-MM-DD")
        }
      })
    }
  }).then(extractDataContent);
};
