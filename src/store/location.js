export default {
  namespaced: true,

  state: {
    lat: null,
    lng: null
  },

  getters: {
    hasLocation: state => state.lat !== null && state.lng !== null,
    getLocation: state => {
      return { lat: state.lat, lng: state.lng };
    }
  },

  mutations: {
    setLocation(state, { lat, lng }) {
      state.lat = lat;
      state.lng = lng;
    }
  },

  actions: {
    setLocation({ commit }, { lat, lng }) {
      commit("setLocation", { lat, lng });
    }
  }
};
