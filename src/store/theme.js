export default {
  namespaced: true,

  getters: {
    themeClasses: (state, getters, rootState, rootGetters) => ({
      "theme-member": rootGetters["account/isLogin"],
      "theme-general-member": rootGetters["account/isGeneralMember"],
      "theme-vip-member": rootGetters["account/isVipMember"],
      "theme-map-member": rootGetters["account/isMapMember"]
      // TODO: 記得改回
      // "theme-vip-member": true
      // "theme-map-member": true,
      // "theme-member": true
    })
  }
};
