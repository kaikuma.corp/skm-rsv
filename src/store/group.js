import _debug from "debug";
import moment from "moment";
import * as Api from "@/api";
import Restaurant from "@/models/restaurant";
import { availableStrategies, isToday } from "@/utils";
import sessionTimeMappings from "@/const/sessionTimeMappings";
const debug = _debug("modules:group");

const resetService = {
  booking: true, // true/false/"disabled"
  waiting: true,
  preorder: true,
  delivery: true
};

export default {
  namespaced: true,
  state: {
    group: null,
    restaurants: [],
    bookingRestaurantList: [],
    loading: false,
    filter: {
      date: moment().format("YYYY-MM-DD"),
      type: "all",
      strategy: "waiting",
      numOfPersons: 2,
      groupId: null,
      service: { ...resetService }
    }
  },
  getters: {
    strategy(state) {
      return state.filter.strategy === "waiting" ? "waiting" : "booking";
    },
    availableStrategies(state) {
      const date = moment(state.filter.date)
        .add(3, "hours")
        .toDate();
      return availableStrategies(state.restaurants, isToday(date), false);
    },
    vipAvailableStrategies(state) {
      const date = moment(state.filter.date)
        .add(3, "hours")
        .toDate();
      return availableStrategies(state.restaurants, isToday(date), true);
    },
    sessionTime(state) {
      return sessionTimeMappings[state.filter.strategy] || {};
    }
  },
  mutations: {
    setGroup(state, group) {
      state.group = group;
      debug("update group data");
    },
    setRestaurants(state, restaurants) {
      state.restaurants = restaurants;
      debug("update restaurants data");
    },
    setLoading(state, bool) {
      state.loading = bool;
    },
    updateFilter(state, val) {
      state.filter = {
        ...state.filter,
        ...val
      };
    },
    updateService(state, val) {
      state.filter.service = {
        ...state.filter.service,
        ...val
      };
    },
    correctStrategy(state, strategies) {
      if (strategies.indexOf(state.filter.strategy) === -1) {
        state.filter.strategy = strategies[0];
        debug("update strategy to", state.filter.strategy);
      }
    },
    clear(state) {
      state.group = null;
      state.restaurants = [];
      state.bookingRestaurantList = [];
      state.filter = {
        date: moment().format("YYYY-MM-DD"),
        type: "all",
        strategy: "waiting",
        numOfPersons: 2,
        groupId: null,
        service: { ...resetService }
      };
    }
  },
  actions: {
    async loadGroup({ commit, state, rootState, getters }) {
      if (!state.filter.groupId || state.loading) {
        return;
      }
      debug("load group data");
      const params = {
        language: rootState.locale,
        numOfPersons: state.filter.numOfPersons,
        date: state.filter.date,
        type: "all"
      };
      commit("setLoading", true);

      const data = await Api.Group.get({
        groupId: state.filter.groupId,
        params
      });

      return (() => {
        const availableStrategies =
          rootState.account.account && rootState.account.account.vip
            ? getters.vipAvailableStrategies
            : getters.availableStrategies;
        commit("setGroup", data);

        // 檢查分店所有的服務
        const defaultDisabledService = {
          booking: "disabled",
          waiting: "disabled",
          preorder: "disabled",
          delivery: "disabled"
        };
        const checkService = data.branches.reduce(
          (acc, restaurant) => ({
            ...acc,
            ...(restaurant.webBookingEnabled && { booking: true }),
            ...(restaurant.webWaitingEnabled && { waiting: true }),
            ...(restaurant.groupMeta.takeOutOnly && { preorder: true }),
            ...(restaurant.groupMeta.SendingOutTag && { delivery: true })
          }),
          {}
        );

        commit("updateService", { ...defaultDisabledService, ...checkService });

        const now = new Date().getTime();
        commit(
          "setRestaurants",
          data.branches
            .filter(
              r =>
                !(
                  (r.groupMeta.startDate && r.groupMeta.startDate > now) ||
                  (r.groupMeta.endDate && r.groupMeta.endDate < now)
                )
            )
            .map(r => new Restaurant(state.filter.date, r))
        );

        commit("correctStrategy", availableStrategies);
        commit("setLoading", false);
      })();
    },

    clearRestaurants({ commit }) {
      commit("setRestaurants", []);
    },

    updateFilter({ commit, dispatch, state, getters }, val) {
      const needToReload = ["groupId", "numOfPersons", "date"].reduce(
        (result, option) => {
          return (
            result ||
            // eslint-disable-next-line no-prototype-builtins
            (val.hasOwnProperty(option) && val[option] !== state.filter[option])
          );
        },
        false
      );

      commit("updateFilter", val);

      if (needToReload) {
        dispatch("loadGroup");
      }
    },

    clear({ commit }) {
      commit("clear");
    }
  }
};
