import qs from "qs";
import _debug from "debug";
import Cookies from "js-cookie";
import * as Api from "@/api";
import Account from "@/models/account";
import Reservation from "@/models/reservation";
import { TAG_VIP, TAG_GENERAL } from "../models/restaurant";
import Vue from "vue";

const debug = _debug("modules:account");

export default {
  namespaced: true,

  state: {
    account: null,
    loading: false,
    inLoginProcess: false,
    loginUri: "",
    menuOpened: false,
    menuPosition: {
      left: 0,
      top: 0
    },
    loadingReservations: false,
    loginMessageDisplayed: Cookies.get("login_message_displayed")
      ? true
      : false,
    loginAction: null,
    cancelAction: null,
    reservations: []
  },

  getters: {
    isLogin: state => Boolean(state.account),
    isVipMember: state => Boolean(state.account && state.account.vip),
    isGeneralMember: state => Boolean(state.account && !state.account.vip),
    isMapMember: state => Boolean(state.account && state.account.mapMember),
    displayLoginMessage: state =>
      !state.loginMessageDisplayed && Boolean(state.account),
    bookingTag: state =>
      state.account && state.account.vip ? TAG_VIP : TAG_GENERAL
  },

  mutations: {
    setAccount(state, { account }) {
      state.account = account ? new Account(account) : null;
      debug("update account: %j", account);
    },

    setLoading(state, loading) {
      state.loading = loading;
    },

    setInLoginProcess(state, inLoginProcess) {
      state.inLoginProcess = inLoginProcess;
    },

    setMenuOpened(state, bool) {
      state.menuOpened = bool;
    },

    setMenuPosition(state, position) {
      state.menuPosition = position;
    },

    setLoginUri(state, payload) {
      state.loginUri = payload;
    },

    setLoginMessageDisplayed(state, bool) {
      state.loginMessageDisplayed = bool;
    },

    setLoginAction(state, loginAction) {
      state.loginAction = loginAction;
    },

    setCancelAction(state, cancelAction) {
      state.cancelAction = cancelAction;
    },

    setLoadingReservations(state, bool) {
      state.loadingReservations = bool;
    },

    setReservations(state, reservations) {
      state.reservations = reservations;
    }
  },

  actions: {
    async fetchAccount({ state, commit }) {
      debug("fetch account");
      commit("setLoading", true);
      const getMemberBasicInfo = await Api.Accounts.getAccount();
      commit("setAccount", { account: getMemberBasicInfo });
      commit("setLoading", false);
      if (getMemberBasicInfo) {
        Vue.rollbar.configure({
          payload: {
            person: {
              id: getMemberBasicInfo.memberId,
              username: getMemberBasicInfo.name
            }
          }
        });
      }
    },

    async fetchReservations({ state, commit }) {
      debug("fetch reservations");
      commit("setLoadingReservations", true);
      Api.Accounts.getRecords(state.account.id)
        .then(responseObj => {
          const { reservations } = responseObj;
          commit(
            "setReservations",
            reservations.map(r => new Reservation(r))
          );
        })
        .catch(err => debug("Failed to load records: ", err))
        .then(() => commit("setLoadingReservations", false));
    },

    async startLogin({ commit, state }, { finishAction, cancelAction } = {}) {
      if (!state.inLoginProcess) {
        debug("start login");
        if (window.skmAppRequestLogin) {
          window.skmAppRequestLogin();
        } else {
          const loginUri = Api.Accounts.getiFrameLoginUri();
          commit("setLoginUri", loginUri);
          commit("setInLoginProcess", true);
          commit("setLoginAction", finishAction || null);
          commit("setCancelAction", cancelAction || null);
        }
      }
    },

    finishLogin({ commit, dispatch, state }) {
      debug("finish login");
      commit("setInLoginProcess", false);
      const loginAction = state.loginAction;
      const cancelAction = state.cancelAction;
      commit("setLoginAction", null);
      commit("setCancelAction", null);
      if (state.account && loginAction) {
        dispatch(loginAction[0], loginAction.slice(1), { root: true });
      }
      if (!state.account && cancelAction) {
        dispatch(cancelAction[0], cancelAction.slice(1), { root: true });
      }
    },

    openAccountMenu({ commit }, position) {
      debug("open account menu");
      commit("setMenuPosition", position);
      commit("setMenuOpened", true);
    },

    closeAccountMenu({ commit }) {
      debug("close account menu");
      commit("setMenuOpened", false);
    },

    toggleAccountMenu({ dispatch, state }, position) {
      if (state.menuOpened) {
        dispatch("closeAccountMenu");
      } else {
        dispatch("openAccountMenu", position);
      }
    },

    logout({ commit }) {
      Api.Accounts.logout()
        .then(() => debug("logout"))
        .then(() => (location.href = "/"))
        .catch(err => debug("Failed to logout: ", err));
      Cookies.remove("preorder_cart");
      commit("setAccount", {});
    },

    showLoginMessage({ commit }) {
      commit("setLoginMessageDisplayed", false);
    },

    closeLoginMessage({ commit }) {
      Cookies.set("login_message_displayed", "1", { expires: 365 * 10 });
      commit("setLoginMessageDisplayed", true);
    }
  }
};
