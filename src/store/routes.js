export default {
  namespaced: true,

  state: {
    updatedTime: Date.now()
  },

  getters: {
    updatedTime: state => state.updatedTime
  },

  mutations: {
    setUpdatedTime(state, updatedTime) {
      state.updatedTime = updatedTime;
    }
  },

  actions: {
    updateUpdatedTime({ commit }) {
      commit("setUpdatedTime", Date.now());
    }
  }
};
