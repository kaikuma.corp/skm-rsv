import _debug from "debug";
import * as Api from "@/api";
import Restaurant from "@/models/restaurant";
import eventBus from "@/event-bus";

const debug = _debug("modules:restaurant");

export const RESERVATION_CREATED = "modules:restaurant:reservationCreated";

export default {
  namespaced: true,
  state: {
    restaurant: undefined,
    loading: true,
    reservation: {
      creating: false,
      id: null,
      link: null,
      error: null
    },
    changePreorderSection: null,
    currentPreorderSection: null,
    preorderSections: []
  },
  getters: {
    waitingInfo({ restaurant }) {
      if (!restaurant) {
        return null;
      }
      return restaurant.waitingInfo;
    }
  },
  mutations: {
    setPreorderSections(state, preorderSections) {
      state.preorderSections = preorderSections;
    },

    setChangePreorderSection(state, section) {
      state.changePreorderSection = section;
      if (section) {
        state.currentPreorderSection = section.key;
      }
    },

    setCurrentPreorderSection(state, key) {
      state.currentPreorderSection = key;
    },

    setRestaurant(state, restaurant) {
      state.restaurant = restaurant;
    },

    setLoading(state, bool) {
      state.loading = bool;
    },

    createReservation(state) {
      state.reservation = {
        id: null,
        link: null,
        error: null,
        creating: true
      };
    },

    clearReservation(state) {
      state.reservation = {
        id: null,
        link: null,
        error: null,
        creating: false
      };
    },

    createReservationFinished(state, reservation) {
      state.reservation = {
        ...reservation,
        creating: false
      };
      eventBus.$emit(RESERVATION_CREATED);
    }
  },
  actions: {
    setPreorderSections({ commit }, sections) {
      commit("setPreorderSections", sections);
    },

    setChangePreorderSection({ commit }, section) {
      commit("setChangePreorderSection", section);
    },

    setCurrentPreorderSection({ commit }, key) {
      commit("setCurrentPreorderSection", key);
    },

    async fetchRestaurant({ commit }, { groupId, companyId, id, params = {} }) {
      commit("setLoading", true);
      const restaurants = await Api.Restaurant.get({
        groupId,
        companyId,
        restaurantId: id,
        params
      });
      return (() => {
        commit("setRestaurant", new Restaurant(params.date, restaurants));
        commit("setLoading", false);
      })();
    },

    clearReservation({ commit }) {
      commit("clearReservation");
    },

    createWaiting({ commit }, data) {
      debug("create reservation: %j", data);
      commit("createReservation");
      Api.Reservation.createWaiting(data).then(
        data => {
          commit("createReservationFinished", {
            error: null,
            link: data.reservationLink,
            id: data.reservationId
          });
        },
        err => {
          debug("failed to create reservation: ", err);
          commit("createReservationFinished", { error: err });
        }
      );
    }
  }
};
