import * as Api from "@/api";
import { API_LOCALE } from "@/i18n";
import { STATUS, RESULT_STATUS } from "@/components/BranchSelector";

export default {
  namespaced: true,

  state: {
    data: {
      branches: [],
      areas: []
    },
    select: {
      branch: "",
      city: "",
      area: "",
      address: ""
    },
    current: STATUS.FORM,
    resultCurrent: RESULT_STATUS.SEARCH,
    groups: [],
    isMatchDeliveryScope: false,
    isDeliveryPopup: false,

    showBranchSelectorPopup: false
  },

  getters: {
    branchOptions(state, getters, rootState) {
      return state.data.branches.map(option => ({
        text: option[`Title${API_LOCALE[rootState.locale]}`],
        value: option.GroupId
      }));
    },
    cityOptions(state, getters, rootState) {
      return state.data.areas.map(option => ({
        // text: option[`CityName${API_LOCALE[rootState.locale]}`],
        text: option[`CityNameZH`],
        value: option["CitiId"]
      }));
    },
    areaOptions(state, getters, rootState) {
      if (state.select.city) {
        const city = state.data.areas.filter(
          item => state.select.city === item["CitiId"]
        );
        const options =
          city.length &&
          city[0]["CityArea"].map(option => ({
            // text: option[`AreaName${API_LOCALE[rootState.locale]}`],
            text: option[`AreaNameZH`],
            value: option["AreaID"]
          }));
        return options;
      }
      return [];
    },
    groupOptions(state, getters, rootState) {
      if (state.groups.length) {
        const result = state.groups.map(branch => ({
          GroupId: branch.GroupId,
          Title: branch[`Title${API_LOCALE[rootState.locale]}`],
          Lat: branch.Lat,
          Lng: branch.Lng
        }));
        return result;
      }
      return [];
    }
  },

  mutations: {
    updateData(state, payload) {
      state.data = payload;
    },
    updateOptions(state, payload) {
      state.options = payload;
    },
    updateSelect(state, payload) {
      state.select = payload;
    },
    updateSelector(state, payload) {
      state.isDeliveryPopup = payload.isDeliveryPopup;
      state.select.companyId = payload.companyId;
      state.select.id = payload.id;
    },
    updateMatchDeliveryScope(state, payload) {
      state.isMatchDeliveryScope = payload;
    },
    changeCurrent(state, payload) {
      state.current = payload.current;
      state.resultCurrent = payload.resultCurrent;
    },
    updateGroups(state, payload) {
      state.groups = payload;
    },
    updateShow(state, payload) {
      state.showBranchSelectorPopup = payload;
    },
    clear(state) {
      state.current = STATUS.FORM;
      state.resultCurrent = RESULT_STATUS.SEARCH;
      state.groups = [];
      state.isMatchDeliveryScope = false;
      state.isDeliveryPopup = false;
      state.select = {
        branch: "",
        city: "",
        area: "",
        address: "",
        companyId: "",
        id: ""
      };
    }
  },

  actions: {
    resetSelectorCurrent({ state, commit }) {
      commit("changeCurrent", {
        current: STATUS.FORM,
        resultCurrent: RESULT_STATUS.SEARCH
      });
    },
    openBranchSelectorPopup({ state, commit }, payload = {}) {
      const { isDeliveryPopup = false, companyId = "", id = "" } = payload;
      commit("changeCurrent", {
        current: STATUS.FORM,
        resultCurrent: RESULT_STATUS.SEARCH
      });
      commit("updateShow", true);
      commit("updateSelector", { isDeliveryPopup, companyId, id });
    },
    closeBranchSelectorPopup({ state, commit, dispatch }, payload) {
      commit("updateShow", false);
      commit("changeCurrent", {
        current: STATUS.FORM,
        resultCurrent: RESULT_STATUS.SEARCH
      });
    },
    // 獲取分店
    async getBranches({ state, commit, dispatch }) {
      const branches = await Api.Branch.getBranches();
      commit("updateData", { ...state.data, branches });
      // dispatch("setBranch");
    },

    // 獲取縣市地區
    async getAreas({ state, commit }) {
      const areas = await Api.Branch.getAreas();
      commit("updateData", { ...state.data, areas });
    },
    updateSelect({ state, commit }, payload) {
      commit("updateSelect", { ...state.select, ...payload });
    },

    // 查詢是否符合外送
    async checkBranchMatchDelivery({ state, commit, dispatch }, payload) {
      const { SearchResult, Groups } = await dispatch("fetchBranch", {
        ...payload
      });
      commit("updateGroups", Groups);
      if (SearchResult) {
        // console.log("sss", payload.groupId);
        dispatch("checkMatchScope", payload.groupId);
        // console.log(state.isMatchDeliveryScope);
        // 符合外送範圍
        state.isMatchDeliveryScope &&
          commit("changeCurrent", {
            current: STATUS.RESULT,
            resultCurrent: RESULT_STATUS.TIME
          });

        // 不符合外送範圍，出現推薦
        !state.isMatchDeliveryScope &&
          commit("changeCurrent", {
            current: STATUS.RESULT,
            resultCurrent: RESULT_STATUS.RECOMMEND
          });
      } else {
        // 不符合外送範圍，也沒有查詢資料
        commit("changeCurrent", {
          current: STATUS.RESULT,
          resultCurrent: Groups.length
            ? RESULT_STATUS.RECOMMEND
            : RESULT_STATUS.NO_RESULT
        });
      }
    },

    checkMatchScope({ state, commit }, payload) {
      if (
        payload &&
        state.select.city &&
        state.select.area &&
        state.select.address
      ) {
        const isMatchDeliveryScope =
          state.groups.filter(({ GroupId }) => GroupId === payload).length > 0;
        // console.log(payload);
        // console.log(state.groups);
        commit("updateMatchDeliveryScope", isMatchDeliveryScope);
        // console.log(isMatchDeliveryScope);
      } else {
        commit("updateMatchDeliveryScope", false);
      }
    },

    async getDeliveryBranches({ dispatch, commit }, payload) {
      const { SearchResult, Groups } = await dispatch("fetchBranch", {
        ...payload
      });

      commit("updateGroups", Groups);
      if (SearchResult) {
        // 有查詢資料
        commit("changeCurrent", {
          current: STATUS.RESULT,
          resultCurrent: RESULT_STATUS.SEARCH
        });
      } else {
        // 無查詢資料，出現推薦
        commit("changeCurrent", {
          current: STATUS.RESULT,
          resultCurrent: Groups.length
            ? RESULT_STATUS.RECOMMEND
            : RESULT_STATUS.NO_RESULT
        });
      }
    },

    // 查詢分店
    async fetchBranch({ state, commit, rootState }, payload) {
      const {
        select: { city, area, address }
      } = state;
      const requestData = {
        City: parseInt(city, 10) || "",
        Area: parseInt(area, 10) || "",
        Address: address || "",
        Lat: payload.lat,
        Lng: payload.lng
      };

      const result = await Api.Branch.searchBranch(requestData);
      return result;
    },

    // 根據geolocation設定分店
    setBranch({ state, commit, rootState }) {
      const { lat, lng } = rootState.location;
      let nearestIndex = 0;
      if (lat && lng) {
        nearestIndex = state.data.branches
          .map(branch => (lat - branch.Lat) ** 2 + (lng - branch.Lng) ** 2)
          .reduce(
            (carry, distance, index, arr) =>
              arr[carry] < distance ? carry : index,
            0
          );
      }

      commit("updateSelect", {
        ...state.select,
        branch: state.data.branches[nearestIndex].GroupId
      });
    }
  }
};
