import Vue from "vue";
import Vuex from "vuex";
import branch from "./branch";
import group from "./group";
import groups from "./groups";
import restaurant from "./restaurant";
import { changeLocale } from "@/i18n";
import location from "./location";
import account from "./account";
import theme from "./theme";
import routes from "./routes";
import _ from "underscore";
import Cookies from "js-cookie";
import _debug from "debug";
import eventBus from "@/event-bus";

import styleVars from "@/assets/stylesheets/_variables.scss";

const mdBreakpoint = parseInt(styleVars.mdBreakpoint);
const debug = _debug("modules:index");
export const CALL_ROUTER_METHOD = "store:index:callRouterMethod";

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    hide: true,
    locale: Cookies.get("locale") || "zh-TW",
    locales: ["zh-TW", "en-US"],
    isMobile: window.innerWidth <= mdBreakpoint,
    navMenuOpened: false,
    windowSize: getWindowSize()
  },
  getters: {},
  mutations: {
    setLocale(state, locale) {
      changeLocale(locale);
      Cookies.set("locale", locale);
      state.locale = locale;
    },

    setIsMobile(state, bool) {
      state.isMobile = bool;
    },

    setNavMenuOpened(state, bool) {
      state.navMenuOpened = bool;
    },

    setHide(state, bool) {
      state.hide = bool;
    },

    setWindowSize(state, size) {
      state.windowSize = size;
    }
  },
  actions: {
    openNavMenu({ commit }) {
      debug("open nav menu");
      commit("setNavMenuOpened", true);
    },

    closeNavMenu({ commit }) {
      debug("close nav menu");
      commit("setNavMenuOpened", false);
    },

    setLocale({ commit }, locale) {
      debug("set locale to ", locale);
      commit("setLocale", locale);
    },

    callRouterMethod(ctx, data) {
      eventBus.$emit(CALL_ROUTER_METHOD, data);
    },

    hide({ commit }) {
      commit("setHide", true);
    },

    show({ commit }) {
      commit("setHide", false);
    }
  },
  modules: {
    branch,
    restaurant,
    group,
    groups,
    location,
    account,
    theme,
    routes
  }
});

window.addEventListener(
  "resize",
  _.throttle(() => {
    store.commit("setIsMobile", window.innerWidth <= mdBreakpoint);
    store.commit("setWindowSize", getWindowSize());
  }, 300)
);

function getWindowSize() {
  const width =
    window.innerWidth ||
    document.documentElement.clientWidth ||
    document.body.clientWidth;
  const height =
    window.innerHeight ||
    document.documentElement.clientHeight ||
    document.body.clientHeight;
  return { width, height };
}

export default store;
