import _debug from "debug";
import api from "@/api/landing";

const debug = _debug("modules:groups");

export default {
  namespaced: true,

  state: {
    groups: [],
    loaded: false
  },

  getters: {},

  mutations: {
    setGroups(state, { groups }) {
      state.groups = groups;
      debug("update groups: %j", groups);
    },

    setLoaded(state) {
      state.loaded = true;
    }
  },

  actions: {
    loadGroups({ commit, state }) {
      if (!state.loaded) {
        debug("load groups");
        api
          .get()
          .then(groups => commit("setGroups", { groups }))
          .catch(err => debug("failed to load groups: ", err));
      }
    }
  }
};
