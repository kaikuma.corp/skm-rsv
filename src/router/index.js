import Vue from "vue";
import Router from "vue-router";
import Landing from "@/pages/Landing";
import Group from "@/pages/Group";
import Restaurant from "@/pages/Restaurant";
import NewReservation from "@/pages/NewReservation";
import ReservationRecords from "@/pages/ReservationRecords";
import PreorderRecord from "@/pages/PreorderRecord";
import PreorderRecords from "@/pages/PreorderRecords";
import SkmpayInfo from "@/pages/SkmpayInfo";
import SkmpayResult from "@/pages/SkmpayResult";
import WaitingForm from "@/pages/WaitingForm";
import List from "@/components/Group/List";
import Preorder from "@/components/Group/Preorder";
import PreorderTutorial from "@/components/Group/PreorderTutorial";
import GroupMap from "@/components/Group/Map";
import TabWaiting from "@/components/Restaurant/TabWaiting";
import TabBooking from "@/components/Restaurant/TabBooking";
import TabPreorder from "@/components/Restaurant/TabPreorder";
import TabDelivery from "@/components/Restaurant/TabDelivery";
import buildingStructure from "@/const/building-structure";

Vue.use(Router);

const defaultGroupId = "skm-xinyi";
const landingEnabled = true;

const metaStopScrolling = {
  stopScrolling: true
};

export default new Router({
  mode: "history",
  routes: [
    landingEnabled
      ? {
          path: "/",
          name: "landing",
          component: Landing
        }
      : {
          path: "/",
          redirect: `/groups/${defaultGroupId}`
        },
    {
      path: "/preorder-tutorial/:anchorId?",
      component: PreorderTutorial,
      name: "PreorderTutorial",
      props: ({ params: { anchorId } }) => ({ anchorId: anchorId })
    },
    {
      path: "/groups/:groupId",
      component: Group,
      props: ({ params: { groupId } }) => ({ id: groupId }),
      beforeEnter: (to, from, next) => {
        const groupId = to.params.groupId;
        if (buildingStructure[groupId]) {
          next();
        } else {
          next(`/groups/${defaultGroupId}`);
        }
      },
      children: [
        { path: "", component: List, name: "Group" },
        { path: "map", component: GroupMap, name: "GroupMap" },
        { path: "preorder", component: Preorder, name: "GroupPreorder" }
      ]
    },
    {
      // TODO: 受限於 API，無法使用 /groups/:groupId/restaurants/:id
      path: "/groups/:groupId/companies/:companyId/restaurants/:id",
      component: Restaurant,
      props: ({
        params: { groupId, companyId, id },
        query: { groupSize, numberOfKidChairs, date, itemid }
      }) => ({
        groupId,
        companyId,
        id,
        groupSize: Number(groupSize),
        numberOfKidChairs,
        date,
        itemid
      }),
      children: [
        {
          path: "waiting",
          component: TabWaiting,
          name: "RestaurantWaiting",
          meta: { ...metaStopScrolling }
        },
        {
          path: "booking",
          component: TabBooking,
          name: "RestaurantBooking",
          meta: { ...metaStopScrolling }
        },
        {
          path: "preorder",
          component: TabPreorder,
          name: "RestaurantPreorder",
          meta: { ...metaStopScrolling }
        },
        {
          path: "delivery",
          component: TabDelivery,
          name: "RestaurantDelivery",
          meta: { ...metaStopScrolling }
        }
      ]
    },
    {
      // TODO: 受限於 API，無法使用 /groups/:groupId/restaurants/:id/reservations/new
      path:
        "/groups/:groupId/companies/:companyId/restaurants/:id/reservations/new",
      name: "NewReservation",
      component: NewReservation,
      props: ({
        params: { groupId, companyId, id },
        query: { groupSize, numberOfKidChairs, date, time, vipSlot }
      }) => ({
        groupId,
        companyId,
        id,
        groupSize: groupSize || 0,
        numberOfKidChairs: numberOfKidChairs || 0,
        date,
        time,
        vipSlot
      })
    },
    {
      // TODO: 受限於 API，無法使用 /groups/:groupId/restaurants/:id/waiting/new
      path: "/groups/:groupId/companies/:companyId/restaurants/:id/waiting/new",
      name: "WaitingForm",
      component: WaitingForm,
      props: ({ params: { groupId, companyId, id } }) => ({
        groupId,
        companyId,
        id
      })
    },
    {
      path: "/account",
      name: "Account",
      redirect: { name: "ReservationRecords" }
    },
    {
      path: "/account/records",
      name: "ReservationRecords",
      component: ReservationRecords
    },
    {
      path: "/account/preorder",
      name: "PreorderRecords",
      component: PreorderRecords
    },
    {
      path: "/account/preorder/:orderId",
      name: "PreorderRecord",
      component: PreorderRecord
    },
    {
      path: "/skmpay/info",
      name: "SkmpayInfo",
      component: SkmpayInfo
    },
    {
      path: "/skmpay/result",
      name: "SkmpayResult",
      component: SkmpayResult
    },
    {
      path: "/skmpay/cancel",
      name: "SkmpayResultCancel",
      meta: { status: "cancel" },
      component: SkmpayResult
    }
  ],
  scrollBehavior(to, from, saved) {
    if (from.meta.stopScrolling && to.meta.stopScrolling) {
      return saved;
    }
    return to.name !== from.name ? { x: 0, y: 0 } : false;
  }
});
