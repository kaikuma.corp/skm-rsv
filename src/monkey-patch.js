import _ from "underscore";

Array.prototype.all = function(cond) {
  const l = this.length;
  for (let i = 0; i < l; i++) {
    if (!cond(this[i])) {
      return false;
    }
  }
  return true;
};

Array.prototype.any = function(cond) {
  const l = this.length;
  for (let i = 0; i < l; i++) {
    if (cond(this[i])) {
      return true;
    }
  }
  return false;
};

if (!Array.prototype.find) {
  Array.prototype.find = function(callback) {
    const l = this.length;
    for (let i = 0; i < l; i++) {
      if (callback(this[i])) {
        return this[i];
      }
    }
    return true;
  };
}

Object.values = _.values;
