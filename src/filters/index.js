import moment from "moment";
import i18n from "../i18n";

const displayDateFormatMappings = {
  "zh-TW": "M/D dd",
  "en-US": "M/D ddd"
};

export const displayDate = date => {
  const isToday = moment(date).isSame(moment(), "day");
  const format = displayDateFormatMappings[i18n.locale];
  return `${moment(date).format(format)} ${
    isToday ? `(${i18n.t("date.today")})` : ""
  }`;
};

export const dateWithWDay = date => {
  return moment(date).format("YYYY/MM/DD ddd");
};

export const floorName = floor => {
  if (typeof floor === "number") {
    return floor > 0 ? `${floor}F` : `B${-floor}`;
  }
  return floor;
};
