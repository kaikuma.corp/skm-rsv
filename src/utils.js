import jsonp from "jsonp";
import moment from "moment";
import _ from "underscore";
import sessionTimeMappings from "@/const/sessionTimeMappings";

export const formatedTime = time => {
  return moment(time, "hh:mm").format("HH:mm");
};

export const isNowBetween = (from, to) => {
  const fromM = moment(from, "hh:mm");
  const toM = moment(to, "hh:mm");
  if (fromM.isAfter(toM)) {
    toM.add(1, "days");
  }
  return moment().isBetween(fromM, toM);
};

export const isTimeBetween = (time, from, to) => {
  const fromM = moment(from, "hh:mm");
  const toM = moment(to, "hh:mm");
  const timeM = moment(time, "hh:mm");
  timeM.add(1, "second");

  if (fromM.isAfter(toM)) {
    toM.add(1, "days");
  }

  return timeM.isBetween(fromM, toM);
};

export const isNowBefore = from => {
  from = moment(from, "hh:mm");
  if (from.isSameOrBefore(moment("03:00", "hh:mm"))) {
    from = from.add(1, "day");
  }
  return moment().isBefore(from);
};

export const isNowAfter = from => {
  from = moment(from, "hh:mm");
  if (from.isSameOrBefore(moment("03:00", "hh:mm"))) {
    from = from.add(1, "day");
  }
  return moment().isAfter(from);
};

export const isToday = date => {
  const start = moment()
    .startOf("day")
    .add(3, "hours");
  const end = start.clone().add(1, "day");
  date = moment(date);
  return date.isSameOrAfter(start) && date.isBefore(end);
};

export const availableStrategies = (restaurants, isToday, isVip) => {
  if (!(restaurants instanceof Array)) {
    restaurants = [restaurants];
  }
  let strategies = [
    "bookingNoon",
    "bookingAfternoon",
    "bookingNight",
    "waiting"
  ];

  if (isToday) {
    strategies = strategies.filter(
      s => s == "waiting" || isNowBefore(sessionTimeMappings[s].to)
    );
  } else {
    strategies = _.without(strategies, "waiting");
  }
  strategies = strategies.filter(
    s =>
      s == "waiting" ||
      restaurants.reduce((slotCount, r) => {
        return slotCount + r.slotsWithin(isVip, sessionTimeMappings[s]).length;
      }, 0)
  );

  return strategies;
};

export const inViewport = (element, options = {}) => {
  options = _.extend(
    { threshold: 0, offset: { top: 0, bottom: 0, left: 0, right: 0 } },
    options
  );

  const {
    top,
    right,
    bottom,
    left,
    width,
    height
  } = element.getBoundingClientRect();

  const intersection = {
    t: bottom,
    r: window.innerWidth - left,
    b: window.innerHeight - top,
    l: right
  };

  const threshold = {
    x: options.threshold * width,
    y: options.threshold * height
  };

  return (
    intersection.t > options.offset.top + threshold.y &&
    intersection.r > options.offset.right + threshold.x &&
    intersection.b > options.offset.bottom + threshold.y &&
    intersection.l > options.offset.left + threshold.x
  );
};

const defaultGeoTimeout = 10000;

export const getGeoLocation = (options = { timeout: defaultGeoTimeout }) => {
  return getGeoLocationFromWebAPI(options);
  //.catch(() => getGeoLocationFallback(options))
};

export const getGeoLocationFallback = (
  options = { timeout: defaultGeoTimeout }
) =>
  new Promise((resolve, reject) => {
    const timer = setTimeout(
      () => reject("FORCE_TIMEOUT"),
      options.timeout + 100
    );
    jsonp("//freegeoip.net/json/", null, (err, data) => {
      clearTimeout(timer);
      if (err) {
        return reject("UNKNOWN_ERROR");
      }
      resolve({
        latitude: data.latitude,
        longitude: data.longitude
      });
    });
  });

export const getGeoLocationFromWebAPI = (
  options = { timeout: defaultGeoTimeout }
) =>
  new Promise((resolve, reject) => {
    if (!window.navigator.geolocation) {
      return reject("POSITION_UNAVAILABLE");
    }
    const timer = setTimeout(
      () => reject("FORCE_TIMEOUT"),
      options.timeout + 100
    );
    const errorHandler = error => {
      clearTimeout(timer);
      switch (error.code) {
        case 1:
          return reject("PERMISSION_DENIED");
        case 2:
          return reject("POSITION_UNAVAILABLE");
        case 3:
          return reject("TIMEOUT");
        default:
          return reject("UNKNOWN_ERROR");
      }
    };
    const successHandler = ({ coords: { latitude, longitude } }) =>
      resolve({ latitude, longitude });
    window.navigator.geolocation.getCurrentPosition(
      successHandler,
      errorHandler,
      options
    );
  });

export const sleep = (time = 1000) =>
  new Promise((resolve, reject) => {
    setTimeout(resolve, time);
  });

export const preorderAvailable = groupId => {
  return {
    "skm-xinyi": 1,
    "skm-KaohsiungZuoying": 1,
    "skm-nanxi": 1,
    "skm-TaipeiStation": 1,
    "skm-taichung": 1,
    "skm-tainanplace": 1
  }[groupId];
};

export const env = {
  ...process.env,
  dev: "development",
  lab: "lab",
  prod: "production"
};

export const restaurantServiceStrategy = (key, restaurant) => {
  if (key === "booking") {
    return restaurant && restaurant.allowBooking;
  } else if (key === "waiting") {
    return !(restaurant && restaurant.waitingDisabled);
  } else if (key === "preorder") {
    return restaurant && restaurant.allowTakeOut;
  } else if (key === "delivery") {
    return restaurant && restaurant.allowSendingOut;
  } else {
    return false;
  }
};

export const last6Char = str => {
  return str.substr(str.length - 6);
};
