import i18n from "@/i18n";
import moment from "moment";

export function preorderTimes(today, restaurants) {
  const times = new Set();
  restaurants.forEach(restaurant => {
    restaurant.preorderTimes(today).forEach(time => {
      times.add(time);
    });
  });
  return Array.from(times).sort((a, b) => {
    return moment(a, "HH:mm") - moment(b, "HH:mm");
  });
}

export function preorderDates() {
  let startDate = moment().startOf("day");
  const endDate = startDate.clone().add(3, "days");
  const dates = [];
  for (; startDate.isBefore(endDate); startDate.add(1, "day")) {
    dates.push(startDate.toDate());
  }
  return dates;
}

export function formatDate(date) {
  const now = moment();
  const momentDate = moment(date);
  return now.isSame(momentDate, "day")
    ? momentDate.format(i18n.t("preorderGroup.formatedPickupTimeToday"))
    : momentDate.format(i18n.t("preorderGroup.formatedPickupTime"));
}
