export const STATUS_AWAIT = 1;
export const STATUS_COMPLETED = 2;
export const STATUS_CANCELED_BY_CUSTOMER = 3;
export const STATUS_READY = 4;
export const STATUS_PREPARING = 5;
export const STATUS_CANCELED_BY_RESTAURANT = 6;
