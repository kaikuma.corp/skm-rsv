import i18n from "@/i18n";

export const LEVEL_VIP = "vip";

export const LEVEL_GENERAL = "general";

export const GENDER_MALE = "male";

export const GENDER_FEMALE = "female";

export default class Account {
  constructor(data) {
    this.data = data;
  }

  get id() {
    return this.data.memberId;
  }

  get name() {
    return this.data.name;
  }

  get phoneNumber() {
    return this.data.phoneNumber;
  }

  get genderType() {
    if (this.data.gender == 0) {
      return GENDER_MALE;
    } else {
      return GENDER_FEMALE;
    }
  }

  get vip() {
    return this.data.vip;
  }

  get mapMember() {
    return this.data.mapMember;
  }

  get level() {
    return this.vip ? LEVEL_VIP : LEVEL_GENERAL;
  }

  get realLevel() {
    return this.data.level;
  }

  get levelTitle() {
    return i18n.t(`account.levelTitle.${this.realLevel}`);
  }

  get gender() {
    return i18n.t(`account.gender.${this.genderType}`);
  }

  get fromApp() {
    return this.data.fromApp;
  }

  get SessionToken() {
    return this.data.SessionToken;
  }
}
