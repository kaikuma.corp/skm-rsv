import moment from "moment";
import { floorName } from "@/filters";

/**
 * @class Order
 * @description 訂單明細資料
 */
export default class Order {
  constructor(data) {
    this.data = data || {};
    this.meta = this.data.meta || {};
  }

  get logo() {
    return this.meta.logo || "";
  }

  get companyName() {
    return this.meta.companyName || "";
  }

  get branchName() {
    return this.meta.branchName || "";
  }

  get groupName() {
    return this.meta.groupName || "";
  }

  get building() {
    return this.meta.building || "";
  }

  get floor() {
    return this.meta.floor.toString() || "";
  }

  get address() {
    return `${this.groupName} ${this.building} ${floorName(this.meta.floor)}`;
  }

  get id() {
    return this.data.id || this.data.mOrderUUID || "";
  }

  get orderType() {
    return this.data.OrderType || this.data.OrderTransType || "";
  }

  get sendingAddress() {
    return this.data.Sending_Address || "";
  }

  get estimatedTime() {
    return this.data.estimatedPickupTime || undefined;
  }

  get shippingFee() {
    return this.data.SendingOut_Fee || 0;
  }

  get totalAmount() {
    return this.data.total_amount || 0;
  }

  get subtotalAmount() {
    return this.shippingFee
      ? this.totalAmount - this.shippingFee
      : this.totalAmount;
  }

  get quantity() {
    return this.data.products.reduce((total, product) => {
      const extrasQuantity = product.extras.reduce(
        (totalExtra, extra) => (totalExtra += extra.items.length),
        0
      );
      return (total += product.quantity + extrasQuantity);
    }, 0);
  }

  get time() {
    const time = moment.unix(this.estimatedTime);
    const today = time.isSame(moment(), "d") ? "(今日)" : "";
    return time.format(`M/D${today} HH:mm`);
  }

  get products() {
    return this.data.products.reduce((acc, product) => {
      return [
        ...acc,
        {
          ...product,
          extras: product.extras.reduce((restructure, { items }) => {
            const destruct = items.map(item => ({ ...item }));
            return [...restructure, ...destruct];
          }, [])
        }
      ];
    }, []);
  }

  get orderCheckoutUrl() {
    return this.data.order_result_url || "";
  }

  get orderResultUrl() {
    return this.data.order_result_url || "";
  }
}
