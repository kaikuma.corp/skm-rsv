import moment from "moment";
import i18n from "@/i18n";

export default class Reservation {
  constructor(data) {
    this.data = data;
  }

  get state() {
    return this.data.state;
  }

  get stateLabel() {
    return i18n.t(`reservation.state.${this.data.type}.${this.state}`);
  }

  get branchName() {
    return this.data.branch.name;
  }

  get branchId() {
    return this.data.branchId;
  }

  get branchAddress() {
    return this.data.branch.address;
  }

  get companyName() {
    return this.data.company.name;
  }

  get companyId() {
    return this.data.companyId;
  }

  get companyLogoStyle() {
    return {
      "background-color": this.data.company.webBannerBackgroundColor,
      "background-image": `url('${this.data.logo}')`,
      "border-color": this.data.company.webBannerBackgroundColor
    };
  }

  get isBooking() {
    return this.data.type === "booking";
  }

  get isWaiting() {
    return this.data.type === "waiting";
  }

  get isWalkIn() {
    return this.data.type === "walk-in";
  }

  get displayTime() {
    return moment(
      this.isBooking ? this.data.reservationTime : this.data.createdTime
    );
  }

  get branchLink() {
    return `/groups/skm-xinyi/companies/${this.companyId}/restaurants/${this.branchId}`;
  }

  get reservationLink() {
    return this.data.reservationLink;
  }

  get customerNote() {
    return this.data.customerNote || i18n.t("reservation.noCustomerNote");
  }

  get adultCount() {
    return this.data.groupSize;
  }

  get childrenCount() {
    return this.data.numberOfKidChairs;
  }

  get stateChangeTime() {
    return moment(this.data.stateChangeTime);
  }
}
