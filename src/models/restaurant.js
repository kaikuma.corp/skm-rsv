import moment from "moment";
import {
  isTimeBetween,
  isNowBetween,
  isNowBefore,
  isNowAfter,
  availableStrategies,
  formatedTime
} from "@/utils";
import i18n from "../i18n";
import { floorName } from "@/filters";
import _ from "underscore";

export const TAG_VIP = "premium";
export const TAG_GENERAL = "default";

export default class Restaurant {
  constructor(date, restaurant) {
    this.date = date;
    this._restaurant = restaurant;

    Object.keys(restaurant).forEach(key => {
      this[key] = restaurant[key];
    });

    if (!this.logo) this.logo = this.company.logo;
  }

  get preorderStrategies() {
    return [
      {
        key: "1",
        name: "早午取餐",
        range: "12:00-14:00"
      },
      {
        key: "2",
        name: "下午取餐",
        range: "14:00-17:00"
      },
      {
        key: "3",
        name: "晚上取餐",
        range: "17:00-20:00"
      }
    ];
  }

  get allPreorderTimes() {
    const pad = n => (n >= 9 ? n : `0${n}`);
    return [
      ...new Set(
        this.openingTimes
          .map(time => {
            const start = parseTime(time.from);
            const end = parseTime(time.to);
            var hours = start.hours;
            var minutes = start.minutes;
            var times = [];
            // eslint-disable-next-line no-constant-condition
            while (1) {
              times.push(`${pad(hours)}:${pad(minutes)}`);
              minutes += 30;
              if (minutes >= 60) {
                minutes = 0;
                hours += 1;
                if (hours >= end.hours) {
                  break;
                }
              }
            }
            return times;
          })
          .reduce((all, times) => all.concat(times), [])
      )
    ];
  }

  preorderTimes(today) {
    const now = moment().add(1, "hours"); // + 1 hours
    const start = moment("12:00", "HH:mm");
    const close = moment("21:30", "HH:mm");
    return this.allPreorderTimes.filter(time => {
      time = moment(time, "HH:mm");
      if (time.isBefore(start)) {
        return false;
      }
      if (today && time.isBefore(now)) {
        return false;
      }
      if (time.isAfter(close) || time.isSame(close)) {
        return false;
      }
      return true;
    });
  }

  get qlieerCName() {
    return this.groupMeta.qlieerCName;
  }

  get preorderEnabled() {
    return Boolean(this.qlieerCName);
  }

  get preorderStarted() {
    return new Date().getHours() >= 12;
  }

  get buildingAddress() {
    if (this.groupName === this.buildingName) {
      return this.buildingAddressWithoutBuilding;
    }
    if (this.buildingName && floorName(this.floor)) {
      return `${this.groupName} ${this.buildingName} ${floorName(this.floor)}`;
    }
    return this.address;
  }

  get buildingAddressWithoutBuilding() {
    return `${this.groupName} ${floorName(this.floor)}`;
  }

  get building() {
    return this.groupMeta.building;
  }

  get buildingName() {
    const key = `building.${this.groupId}.building${this.buildingId}`;
    return i18n.t(key) === key || !i18n.t(key) ? this.building : i18n.t(key);
  }

  get buildingId() {
    return this.groupMeta.STORENO || "";
  }

  get groupName() {
    return i18n.t(`group.${this.groupId}`);
  }

  get floor() {
    return this.groupMeta.floor;
  }

  typeFilter(tags) {
    return tags
      .map(tag => tag.id)
      .filter(typ =>
        [
          "chinese",
          "japanese",
          "korean",
          "american",
          "vegi",
          "southeastasia",
          "italian",
          "hk",
          "bbq",
          "cafe",
          "teppanyaki",
          "hotpot",
          "steak",
          "allucaneat",
          "family"
        ].includes(typ)
      );
  }

  get restaurantTypes() {
    return this.typeFilter(this.company.tags);
  }

  get restaurantType() {
    const types = this.typeFilter(this.company.tags);
    return types.length && types[0] ? types[0] : undefined;
  }

  get slots() {
    if (this.date) {
      return Object.entries(
        this.bookingInfo[TAG_GENERAL][this.date] || {}
      ).map(([time, status]) => ({ time, active: status == "open" }));
    }
    return [];
  }

  get vipSlots() {
    var slots = [];
    if (this.date) {
      slots = this.slots;
    }
    slots = slots.map(slot => {
      if (!slot.active) {
        const info = this.bookingInfo[TAG_VIP] || {};
        const date = info[this.date] || {};
        slot.active = date[slot.time] == "open";
        slot.vipSlot = true;
      }
      return slot;
    });
    return slots;
  }

  get realMapPosition() {
    return {
      x: this.groupMeta.x || 800,
      y: this.groupMeta.y || 800
    };
  }

  availableDate() {
    return bookingInfoToAvailableDate(this.bookingInfo[TAG_GENERAL] || {});
  }

  vipAvailableDate() {
    const general = bookingInfoToAvailableDate(
      this.bookingInfo[TAG_GENERAL] || {}
    );
    const vip = bookingInfoToAvailableDate(this.bookingInfo[TAG_VIP] || {});
    Object.entries(vip).forEach(([key, val]) => {
      general[key] = true;
    });
    return general;
  }

  get inOperation() {
    return (
      this.alwaysOpen ||
      this.openingTimes.any(ot => isNowBetween(ot.from, ot.to))
    );
  }

  get firstOpeningTime() {
    return this.openingTimes[0] || {};
  }

  get nextOpeningTime() {
    return this.openingTimes.find(ot => isNowBefore(ot.from));
  }

  get openWaiting() {
    return this.waitingInfo.status == "open";
  }

  get dayOff() {
    return !this.openingTimes.length;
  }

  get alwaysOpen() {
    if (this.openingTimes.length == 2) {
      const times = this.openingTimes.map(ot => ({
        from: formatedTime(ot.from),
        to: formatedTime(ot.to)
      }));
      return (
        times[0].from != times[0].to &&
        times[0].from == times[1].to &&
        times[0].to == times[1].from
      );
    }
    return false;
  }

  get isAfterFirstOpenTime() {
    if (this.openingTimes.length) {
      return this.alwaysOpen || isNowAfter(this.openingTimes[0].from);
    }
    return false;
  }

  get inRestTime() {
    return this.isAfterFirstOpenTime && this.nextOpeningTime;
  }

  get inClosingTime() {
    return this.openingTimes.length && !this.nextOpeningTime;
  }

  get waitingOrder() {
    var order = this.id === "-L0PaqaJcEoWqvypI2_K" ? -999 : 0;

    if (!this.closed && this.waitingCount) {
      order -= this.waitingCount + 1000;
    }

    return order;
  }

  bookingOrder(vip) {
    if (this.id === "-L0PaqaJcEoWqvypI2_K") {
      return -Infinity;
    }
    return vip ? -this.vipAvailableSlots.length : -this.availableSlots.length;
  }

  // Waiting Info
  get estimatedWaitingMinutes() {
    return this.waitingCount > 0 ? this.waitingInfo.estimatedWaitingMinutes : 0;
  }

  get waitingCount() {
    return this.waitingInfo.waitingCount;
  }

  get closed() {
    return !this.openWaiting || !this.inOperation;
  }

  get longWaiting() {
    return this.waitingInfo.estimatedWaitingMinutes >= 180;
  }

  // Booking Info
  get availableSlots() {
    return this.slots.filter(slot => slot.active);
  }

  get vipAvailableSlots() {
    return this.vipSlots.filter(slot => slot.active);
  }

  slotsWithin(vip, { from, to }) {
    const slots = vip ? this.vipSlots : this.slots;
    if (from && to) {
      return slots.filter(slot => isTimeBetween(slot.time, from, to));
    }
    return slots;
  }

  get allowTakeOut() {
    return this.groupMeta.takeOutOnly;
  }

  get allowSendingOut() {
    return this.groupMeta.SendingOutTag;
  }

  get allowBooking() {
    return this.maxBookingGroupSize > 0 && this.webBookingEnabled;
  }

  get availableBookingStrategies() {
    return _.without(availableStrategies(this, false, false), "waiting");
  }

  get vipAvailableBookingStrategies() {
    return _.without(availableStrategies(this, false, true), "waiting");
  }

  // images
  get logoXS() {
    return `${this.logo}?fit=clip&w=100&h=100`;
  }

  get logoSM() {
    return `${this.logo}?fit=clip&w=200&h=200`;
  }

  get thumbnailSM() {
    return `${this.images[0]}?fit=clip&w=400&h=400`;
  }

  get dataForMixpanel() {
    return {
      company_id: this.company.id,
      company_name: this.company.name,
      branch_id: this.id,
      branch_name: this.name,
      group_id: this.groupId
    };
  }

  get waitingDisabled() {
    const companies = [
      "-KNf3CgtuFU5cea5_nVg", // 鼎泰豐
      "-KpZYK_7r8Cymof4xnZN", // GB
      "-L36k4RLcQz0pL4FOIT7:inline-live-woosa" // woosa
    ];
    return (
      !!~companies.indexOf(this.company.id) || this.webWaitingEnabled === false
    );
  }

  thumbnail(width, height, q = 90) {
    return this.images[0] + `?fit=crop&w=${width}&h=${height}&q=${q}`;
  }
}

function bookingInfoToAvailableDate(bookingInfo = {}) {
  return Object.entries(bookingInfo)
    .filter(([key, val]) => Object.keys(val).reduce((r, v) => r || v, false))
    .reduce((obj, [key, val]) => ((obj[key] = true), obj), {});
}

function parseTime(range) {
  const parts = range.split(":").map(n => Number(n));
  return { hours: parts[0], minutes: parts[1] };
}

function parseTimeRange(range) {
  const parts = range.split("-").map(n => parseTime(n));
  return { start: parts[0], end: parts[1] };
}

function createRangeFilter(range) {
  range = parseTimeRange(range);
  return time => {
    time = parseTime(time);
    return (
      range.start.hours <= time.hours &&
      (range.start.hours !== time.hours ||
        range.start.minutes <= time.minutes) &&
      range.end.hours >= time.hours &&
      (range.end.hours !== time.hours || range.end.minutes >= time.minutes)
    );
  };
}

function filterTimes(times, range) {
  const filter = createRangeFilter(range);
  return times.filter(filter);
}
