export default scroll => {
  const listener = e => {
    if (!("deltaX" in e)) {
      return;
    }
    const dx = Math.abs(e.deltaX);
    const dy = Math.abs(e.deltaY);
    if (dx > 0 && dy < dx) {
      const hasVerticalScroll = scroll.hasVerticalScroll;
      scroll.hasVerticalScroll = true;
      scroll._onMouseWheel({
        deltaMode: e.deltaMode,
        deltaX: e.deltaX,
        deltaY: 0,
        preventDefault: e.preventDefault.bind(e)
      });
      scroll.hasVerticalScroll = hasVerticalScroll;
    }
  };
  scroll.wrapper.addEventListener &&
    scroll.wrapper.addEventListener("wheel", listener);
  scroll.on("destroy", () => {
    scroll.wrapper.removeEventListener &&
      scroll.wrapper.removeEventListener("wheel", listener);
  });
  scroll.firstWheelOpreation = true;
  return scroll;
};
