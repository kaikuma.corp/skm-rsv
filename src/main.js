// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import "babel-polyfill";
import Vue from "vue";
import "./rollbar";
import lineClamp from "vue-line-clamp";
import VueResource from "vue-resource";
import VueScrollTo from "vue-scrollto";
import VueDragscroll from "vue-dragscroll";
import Inview from "vueinview";
import App from "./App";
import router from "./router";
import store, { CALL_ROUTER_METHOD } from "./store";
import i18n from "./i18n";
import * as filters from "./filters";
import "./monkey-patch";
// import "font-awesome/css/font-awesome.css";
import eventBus from "./event-bus";
import "./registerServiceWorker";
import { env } from "./utils";

Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key]);
});
Vue.use(lineClamp);
Vue.use(VueResource);
Vue.use(VueScrollTo);
Vue.use(Inview);
Vue.use(VueDragscroll);

Vue.config.productionTip = false;
Vue.http.options.root = env.VUE_APP_ENV;

/* eslint-disable no-new */

new Vue({
  router,
  el: "#app",
  store,
  i18n,
  render: h => h(App)
});

eventBus.$on(CALL_ROUTER_METHOD, data => {
  router[data[0]](...data.slice(1));
});
