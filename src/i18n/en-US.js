const landing = {
  title: "Shin Kong Mitsukoshi Gourmet Map",
  subTitle:
    "Recommend you restaurants and offer reservation / waiting / take-out / delivery service.",
  available: {
    title: "Currently available: ",
    delimeter: ","
  },
  loadingMask: {
    locating: "Locating"
  },
  button: {
    booking: "Make reservation",
    preordering: "Takeout"
  }
};

const branchSelector = {
  or: "or",
  title: "The branch you want to enjoy the gourmet",
  pleaseSelect: "請選擇分店",
  select: "Search the branch",
  noResult: "Your address is available for the delivery service.",
  submit: "進入美食地圖",
  unavailable: "(未開放)",
  searchSelector: {
    title: "Search the branch available for the food delivery service",
    deliverTitle: "Search the branch available for the food delivery service",
    resultTitle: "Choose your favorite branch for food delivery service",
    resultTitle2: "Your address is available for the delivery service.",
    choose: "Your address is available for the delivery service.",
    chooseTime: "Please choose time.",
    noResult:
      "Your address is not in the delivery service area. We recommend you choose the branch near where you are for the reservation / waiting / take-out service.",
    noResult2: "{recommend}",
    resultHasRecommend:
      "Your address is not in the delivery service area. Please choose the branch near you, fill in the address again or choose another service.",
    resultNoRecommend:
      "Your address is not in the delivery service area. Please fill in the address again or choose another service.",
    otherService: "您可以選擇其他服務"
  },
  button: {
    goNow: "Go",
    checkBranch: "Search",
    back: "Back",
    back2: "變更",
    confirm: "Submit",
    goDelivery: "Go"
  }
};

const placeholder = {
  select: "請選擇",
  selectCity: "Choose the city / county",
  selectArea: "Choose the district",
  selectBranch: "Search the branch",
  input: "請輸入${postfix}",
  address: "Fill in the address in Chinese"
};

export default {
  placeholder,

  landing,
  branchSelector,

  records: {
    pageTitle: "Reservation status",
    noBookingRecords: "No Reservations",
    noWaitingRecords: "No Reservations",
    count: "{n} records",
    breadcrumb: "Member Center",
    waiting: {
      tab: "Waiting ({n})"
    },
    booking: {
      tab: "Booking ({n})"
    },
    walkIn: {
      tab: "Others ({n})"
    },
    groupSize: "{adultCount} adult {childrenCount} kid",
    restaurantButton: "Restaurant Page",
    reservationButton: "Reservation Detail",
    desktopTable: {
      time: "Created Time",
      name: "Restaurant",
      state: "State",
      seatedTime: "Seated Time",
      groupSize: "Amount of people",
      note: "Note",
      link: "Reservation Detail"
    }
  },

  reservation: {
    state: {
      waiting: {
        waiting: "Waiting",
        cancelled: "Cancelled",
        seated: "Seated"
      },
      booking: {
        waiting: "Waiting",
        cancelled: "Cancelled",
        seated: "Seated"
      },
      "walk-in": {
        waiting: "Waiting",
        cancelled: "Cancelled",
        seated: "Seated"
      }
    },
    noCustomerNote: "Empty"
  },

  account: {
    hi: "Hi, dear {level} owner <strong>{name}</strong>",
    levelTitle: {
      N: "Member-card Owners",
      S: "Member-card Owners",
      V: "Premium-card Owners",
      "0": "Member"
    },
    menu: {
      logout: "Log-out",
      reservationRecords: "Reservation status",
      preorder: "Takeout status",
      cardDetail: "Member's treatment"
    },
    gender: {
      male: "Mr.",
      female: "Mrs."
    },
    vipTreatment: {
      treatment1: {
        title: "Treatment 1",
        content: "Booking a reserved table"
      },
      treatment2: {
        title: "Treatment 2",
        content: "Put your name in the waiting list at front"
      }
    }
  },

  navMenu: {
    title: "SKM Gourmet Map",
    skmHomePage: "Home page"
  },

  "site-name": "Shinkong Mitsukoshi Gourmet Map",
  nav: {
    languages: {
      "zh-TW": "繁中",
      "en-US": "English",
      mb: {
        "zh-TW": "繁",
        "en-US": "EN"
      }
    },
    login: {
      desktopButton: "Log-in",
      mobileButton: "Log-in"
    }
  },
  header: {
    menu: {
      discovery: "Eat now!",
      foodMap: "Floor guide"
    },
    button: {
      "goto-nanxifoodmap": "Nanxi Food Map",
      "goto-skm-nanxi": "Taipei Nanxi Store"
    }
  },
  group: {
    nanxifoodmap: "Nanxi Food Map",
    "skm-xinyi": "Xinyi Place",
    "skm-taichung": "Taichung Place",
    "skm-tainanplace": "Tainan Place",
    "skm-nanxi": "Taipei Nanxi Store",
    "skm-TaipeiStation": "Taipei Station Store",
    "skm-TaipeiTianmu": "Taipei Tienmu",
    "skm-KaohsiungZuoying": "Kaohsiung Zuoying",
    "skm-KaohsiungSanduo": "Kaohsiung Sanduo",
    "skm-TainanZhongshan": "Tainan Zhongshan",
    "skm-TaoyuanStation": "Taoyuan Station",
    "skm-TaoyuanDayou": "Taoyuan Dayou",
    "skm-ChiayiChuiyang": "Chiayi Chuiyang"
  },
  groupDescription: {
    nanxifoodmap:
      "Located next to the MRT Zhongshan station, combined with unique street culture and arts and humanities, with secret restaurants selected, providing the dining environment for friends gathering.",
    "skm-xinyi":
      "Located in Xinyi Special District, the most refined city area in Taipei. There are 46 selected restaurants and café for you to choose in bulding A4, A8, A9 and A11.",
    "skm-taichung":
      "SKM TaiChung is the marquis shopping mall in central Taiwan. Enjoy lunch and dinner with great meals, interesting themes, and top-rated reviews. For late night fun and fantastic views, our top-floor establishments offer well-made cocktails, beers, and American-style food.",
    "skm-nanxi":
      "Located in Taipei fashion seminary - Zhongshan Commercial District, it provides Taiwanese authentic delicacies and the most popular restaurants in Taipei.",
    "skm-tainanplace":
      "The first large scale department store in southern Taiwan, satisfying every gourmet's all day dining choice with the most popular restaurants.",
    "skm-TaipeiStation":
      "Located in transport hub of Taiwan High Speed Rail, Taiwan Railways, Taipei Rapid Transit System, Taoyuan Airport MRT and Highway bus, the most important in landmark in western Taipei.<br /><br />There are various types of cuisines in the restaurants on B1F, 11F and 12F, providing the fastest and most convenient choices for tourists or business dinner.",
    "skm-TaipeiTianmu":
      "Located in the most exotic Tienmu business district. Both building A and B provide various types of foreign cuisines. Either the sense of taste or sight makes people feel as if they are in a foreign country.",
    "skm-KaohsiungZuoying":
      "Located in transport hub of Taiwan High Speed Rail, Taiwan Railways and Kaohsiung Rapid Transit System, with the most popular delicious cuisines and restaurants selected, providing all day dining choices for every tourist.",
    "skm-KaohsiungSanduo":
      "Located in the liveliest business district, Sanduo business district, in southern Kaohsiung, the best place for family altogether to go shopping and enjoy delicious cuisines.",
    "skm-TainanZhongshan":
      "The most fashionable department store in front of Tainan TRA station, with the most popular delicious cuisines and restaurants selected, customers either dine alone or together can be satisfied.",
    "skm-ChiayiChuiyang":
      "Located in crowded Chuiyang business district in Chiayi city, with well-selected cuisines and restaurants, providing the best dining places for friends and families.",
    "skm-TaoyuanDayou":
      "Located in Taoyuan Dayou special district, near Nankan and Taiwan Taoyuan International Airport, the best place to enjoy the gourmets.",
    "skm-TaoyuanStation":
      "Located in front of Taoyuan TRA station at Taoyuan commercial center, with the most popular delicious cuisines and restaurants selected, providing the best and most convenient choice of business dinner for tourists."
  },
  breadcrumb: {
    root: "Gourmet Map",
    preorderRecords: "Takeout Status",
    takeout: "Takeout",
    reservation: "Reservation",
    waitingStatus: "Waiting Status"
  },
  building: {
    restaurantList: "All restaurants in {buildingName}",
    floor: "{floor} Floor",
    restaurantsCount: "{count} restaurants",
    "skm-xinyi": {
      building121: "",
      building122: "",
      building123: "",
      building124: ""
    },
    "skm-taichung": {
      main: "Taichung Place",
      building201: ""
    },
    "skm-tainanplace": {
      building321: "Tainan Place",
      building322: "Tainan Place"
    },
    "skm-nanxi": {
      building101: "Building 1",
      building102: "Building 2",
      building103: "Building 3"
    },
    "skm-TaipeiTianmu": {
      building151: "A館",
      building152: "B館"
    },
    "skm-TaipeiStation": {
      building111: "Taipei Station"
    },
    "skm-KaohsiungSanduo": {
      building301: "Kaohsiung Sanduo"
    },
    "skm-TainanZhongshan": {
      building311: "Tainan Zhongshan"
    },
    "skm-KaohsiungZuoying": {
      building351: "Kaohsiung Zuoying",
      building352: "彩虹市集"
    },
    "skm-ChiayiChuiyang": {
      building331: "Chiayi Chuiyang"
    },
    "skm-TaoyuanDayou": {
      building161: "Taoyuan Dayou"
    },
    "skm-TaoyuanStation": {
      building171: "Taoyuan Station"
    }
  },
  restaurant: {
    tab: {
      booking: "Booking",
      waiting: "Waiting",
      preorder: "Takeout",
      delivery: "Delivery"
    },
    title: {
      waiting: "Waiting status",
      booking: "Booking status",
      preorder: "Preorder"
    },
    orderTitle: "Please select your meal",
    bookingAnnouncement: "Announcement",
    readMore: "Read more",
    readLess: "Read less",
    preorderButton: "Order now",
    today: "{date} (Today)",
    waitingLine: "Waiting line:",
    info: "Restaurant information",
    location: "Restaurant location",
    type: "Cuisine type",
    minimumCharge: "Minimum spending per person",
    phone: "Phone",
    openingTime: "Opening hours",
    acceptablePaymentOption: "Payment methods",
    acceptablePaymentOptions:
      "Cash, skm pay, vouchers, credit card (VISA, MASTERCARD, JCB, AE, UnionPay, debit card, Apple Pay, Android Pay, Samsung Pay), LINE Pay, WeChat Pay, Alipay",
    menu: "Menu",
    waitingAndBooking: "Waiting & Booking Status",
    recommendedRestaurants: "Other recommendations",
    waitingTime: "Waiting for {minutes} minutes",
    waitingTime2: "Wait for {minutes} minutes",
    longWaiting: "Long time waiting",
    waitingCount: "Current groups in line",
    waitingCountDesc: "{waitingCount}  groups in line",
    iWantToWait: "Get in the waiting list",
    iWantToWaitVip: "Get in the waiting list at front",
    goToPickNumber: "Please go to the restaurant for gettign in line",
    notAllowBooking: "On-line reservation is disabled<br>to this restaurant",
    waitingDisclaimer:
      "Please contact the restaurant hostess for actual wait time and groups in line",
    iDontWantToWait: "Don't want to wait?",
    types: {
      Thai: "Thai",
      undefined: "Undefined"
    },
    noWaiting: "Currently available",
    noWaitingDesc: "Please go to the restaurant directly for a seat",
    waitingStatusClosed: "Waiting is currently closed",
    closed: "Currently not open",
    temporarilyClosed: "Closed",
    waitingTemporarilyClosedLine1: "Closed",
    waitingTemporarilyClosedLine2: "",
    notOpen: "Not Open",
    closedDesc: "Open at {nextOpenTimeFrom}",
    dayOff: "Off Today",
    noNextOpenTime: "Closed for today",
    waitingStatus: {
      closed: "(Waiting is closed)",
      nowAvailable: "Currently available"
    },
    noSlotAtSession: "Fully booked for the selected date",
    bookingDisabled: "Booking is not available",
    floor: "{floor} Floor",
    preorderClosed: "Closed",
    preorderUnavailable: "Unavailable",
    preorderNotStarted: "Please select time",
    preorderDesc: "@:restaurantPreorderTimeSelector.desc",
    deliveryDesc: "@:restaurantDeliveryTimeSelector.desc"
  },
  date: {
    today: "Today"
  },
  list: {
    close: "Temporary closed",
    unavailable: "Booking unavailable",
    disabledPreorder: "Temporary closed"
  },
  filters: {
    numOfPersons: "Persons",
    personUnit: "",
    date: "Date",
    strategies: {
      waiting: "Waiting",
      bookingNoon: "Lunch",
      bookingAfternoon: "Afternoon",
      bookingNight: "Dinner"
    }
  },
  restaurantTypes: {
    all: "All",
    chinese: "Chinese",
    japanese: "Japanese",
    korean: "Korean",
    american: "American",
    vegi: "Vegitarian",
    southeastasia: "South East Asia",
    italian: "Italian",
    hk: "Hong Kong",
    bbq: "BBQ",
    cafe: "Café",
    teppanyaki: "Teppanyaki",
    hotpot: "Hot Pot",
    steak: "Steak",
    allucaneat: "All You Can Eat",
    family: "Family",
    mall: "Mall",
    undefined: "No valid category"
  },
  actions: {
    book: "Booking",
    back: "Back",
    submit: "Send",
    next: "Next",
    viewReservation: "Check booking info",
    backToFoodMap: "Back to homepage"
  },
  sorry: {
    noSlot: "Not available for this time slot, please check the other ones",
    noSlotAllDay: "Not available for this date, please check the other ones"
  },
  bookingForm: {
    title: "Booking info",
    info: "Booking info",
    numOfPersons: "@:filters.numOfPersons",
    date: "@:filters.date",
    time: "Time",
    name: "Reservation name",
    namePlaceholder: "Joe",
    phone: "Phone number",
    phonePlaceholder: "0912345678",
    phoneHint: "（A SMS will be sent if booking success）",
    purpose: "Purpose",
    sizeLimitHint:
      "Able to book online up to {size} people. Please call the restaurant to book for a big party",
    nAdult: "{n} adult",
    nKid: "{n} kid",
    nAdultNKid: "{adult} adult {kid} kid",
    gender: {
      mr: "Mr.",
      miss: "Miss."
    },
    purposes: {
      celebration: "Birthday",
      dating: "Date",
      annualSale: "Anniversery",
      family: "Family gathering",
      friend: "Friends gathering",
      business: "Business"
    },
    note: "Remark",
    notePlaceholder:
      "Anything that we can help? Leave it here to us!（e.g.: Inconvinent when moving, food alergy, etc）",
    additionalRequirement: "Additional requirements",
    additionalRequirements: {
      kidSet: "Child Seat",
      kidSetUnit: "",
      pram: "Baby Carriage",
      pramUnit: "",
      hint: "Above requirements may valid based on the restaurant's condition"
    },
    result: {
      success: "Booking success",
      failure: "Booking fail",
      smsToPhone: "A SMS has been sent to {phone}"
    },
    customerInfo: "Contact information",
    editContactInfo: "Edit"
  },
  waitingForm: {
    waitingGroupSize: "Amount of people to get in the waiting list",
    nAdult: "{n} adult",
    nKid: "{n} kid",
    maxGroupSize: "Maximum {n} people to get in the waiting list",
    contactInfo: "Contact informaton",
    contactInfoDesc: "(A SMS will be sent if booking success)",
    contactName: "Name of register",
    contactPhone: "Phone number",
    editContactInfo: "Edit contact information",
    note: "Remark to the waiting list",
    notePlaceholder:
      "Put your requirements here if any. (e.g. no beef, vegetarian, etc)",
    additionalRequirement: "Additional requirements",
    additionalRequirements: {
      kidSet: "Child Seat",
      kidSetUnit: "",
      pram: "Baby Carriage",
      pramUnit: "",
      hint: "Above requirements may valid based on the restaurant's condition"
    },
    submit: "Send",
    back: "Back",
    succeeded: {
      message1: "Waiting success",
      message2:
        'The groups in waiting list currently is <span class="count">{count}</span> .<br />Because you are our premium member, your seats will be arranged in high priority.'
    },
    smsSent: "A SMS has been sent to {phone}",
    viewReservation: "Check waiting info",
    backToHome: "Back to homepage",
    vipTreatment: {
      title: "Member Treatment",
      content: "Put your name in the waiting list at front"
    }
  },
  loginMessage: {
    general: {
      title: "Hi, dear member-card owner <br /><strong>{name}</strong>",
      message:
        "Welcome to the SKM Gourmet Map. Now member-card owners are able to put names in the restaurants waiting lists!",
      closeButton: "Got it"
    },
    vip: {
      title: "Hi, dear premium-card owner <br /><strong>{name}</strong>",
      message:
        "Welcome to the SKM Gourmet Map. Premium-card owners will have the 1st class treatments.",
      closeButton: "Got it"
    }
  },
  loginBanner: {
    booking: {
      title: "Log-in and make a reservation now!",
      desc:
        "Member-card owners can reserve a table online. Log-in your SKM member account now!"
    },
    waiting: {
      title: "Log-in and make a reservation now!",
      desc:
        "Member-card owners can reserve a table online. Log-in your SKM member account now!"
    }
  },
  groupTabs: {
    booking: "Eat now!",
    preordering: "Takeout",
    map: "Floor guide",
    map2: "Map guide"
  },
  preorderDateSelector: {
    cancel: "Cancel",
    confirm: "Done",
    title: "Time"
  },
  preorderRecords: {
    cancelConfirm: "訂單一經取消後，則無法復原。您確認要取消此筆訂單？",
    cancelSuccess:
      "訂單取消完成！我們將於7-10個工作天內進行退款申請作業，款項將退至您原付款之信用卡帳戶，實際入帳日期將視您的發卡銀行或信用卡公司作業時間有所不同。",
    cancelError:
      "您的訂單已成立，若有需要其他服務請使用聯絡客服功能，我們盡快與您聯繫。",
    title: "Takeout Status",
    newRecords: "New orders ({count})",
    historyRecords: "Order history ({count})",
    total: "Total {count}",
    pickUpRecords: "外帶紀錄 ({count}筆)",
    deliverRecords: "外送紀錄 ({count}筆)"
  },
  preorderContact: {
    title: "Customer service",
    detailTitle: "Your order",
    restaurant: "Restaurant",
    time: "Takeout time",
    number: "Order number",
    status: "Order status",
    cancelledDesc: "Refund processing (Cancelled by the restaurant)",
    email: "Your email (*)",
    emailPlaceholder: "Email",
    content: "Content (*)",
    contentPlaceholder: "Adjust or cancel order, or other requirements",
    desc:
      "Please send your demand or questions. We will get back to you soon as we can.",
    submit: "Submit",
    successMessage:
      "Please send your demand or questions. We will get back to you soon as we can.",
    close: "Close"
  },
  preorderQrCode: {
    number: "Order number",
    desc:
      "Please show the QR code to the clerk to pick up, or show the order number."
  },
  preorderRecordItem: {
    pickupTime: "{time} Takeout",
    deliveryTime: "{time} Delivered",
    number: "Order number {number}",
    points: "Reward points",
    cancel: "Cancel order",
    contact: "Customer service",
    view: "See order details"
  },
  preorderRecordList: {
    orderTime: "Order time",
    restaurant: "Restaurant",
    pickupTime: "Takeout time",
    number: "Order number",
    amount: "Price",
    status: "Status",
    action: "Action",
    cancel: "Cancel order",
    contact: "Customer service",
    view: "See order details",
    noRecords: "No takeout records"
  },
  preorderPickup: {
    title: "Order complete",
    desc: "Thank you for the waiting. Enjoy your meal.",
    back: "Back to takeout status"
  },
  preorderStatus: {
    1: "Await order to be accepted",
    2: "Completed",
    3: "Canceled",
    4: "Ready to pick up ",
    5: "Preparing your meal",
    6: "Canceled",
    7: "等待外送取餐",
    8: "送餐中",
    N: "訂單未完成",
    Y: "Completed",
    crossDate: "Preparing your meal"
  },
  preorderCart: {
    title: "Cart details",
    amount: "Total",
    count: "{count} items in the cart"
  },
  preorderCartPanel: {
    title: "My cart",
    restaurant: "Restaurant",
    pickupTime: "Takeout time",
    yourOrder: "Your order items",
    amount: "Total",
    note: "Remark",
    notePlaceholder: "Input your demands here for requirements to your order.",
    submit: "Check out",
    remove: "Remove",
    productNote: "Remark：{note}",
    timeSelectorDesc:
      "*Pick-up time: 12:00 noon to 8:00 PM on the booked date.",
    desc:
      "*In order to keep the food quality at its best and the storage clean, please pick up your food 20 minutes within’ the pick-up time you booked.",
    total: "Total"
  },
  deliveryCartPanel: {
    restaurant: "Restaurant",
    pickupTime: "Delivery time",
    desc:
      "For the quality, all the food are made right after your order. During peak hour, the food might be delivered over the estimated time.",
    charge: "charge",
    timeSelectorDesc:
      "*Delivery time: 12:00 noon to 8:00 PM on the booked date."
  },
  preorderProductPanel: {
    note: "Requirements",
    notePlaceholder: "Flavor preferences or allergy",
    updateCart: "Update your cart",
    addToCart: "Add to cart",
    unavailableMessage:
      "Takeout service opens from 11:00. The menu can only be viewed when it is closed.",
    unavailableMessage2: "This product is currently sold out.",
    limitedQty: "Maxima quantity can order per item is 8.",
    cartInCount: "There is already {count} in the cart."
  },
  deliveryProductPanel: {
    limitedQty:
      "This order is reached the delivery volume of one delivery man. Please take another new order after completing this one."
  },
  restaurantPreorder: {
    orderButton: "Takeout",
    readOnlyOrderButton: "Meal details"
  },
  restaurantPreorderNav: {
    pickupTime: "Takeout time",
    order: "Takeout menu"
  },
  restaurantDeliveryNav: {
    pickupTime: "Delivery time",
    order: "Please select your meal"
  },
  cartPanel: {
    unavailableTimeMessage: "由於作業已逾時，煩請重新選擇取餐時間"
  },
  restaurantPreorderTimeSelector: {
    label: "Takeout time",
    date: "{today}（Today)",
    desc:
      "*Pick-up time: 12:00 noon to 8:00 PM on the booked date. <br />*In order to keep the food quality at its best and the storage clean, please pick up your food 20 minutes within’ the pick-up time you booked.",
    time: "Please select takeout time",
    order: "Please select your meal",
    closed: "Currently not available",
    unavailable: "Takeout service opens from 11:00 of the day."
  },
  restaurantDeliveryTimeSelector: {
    label: "Delivery time",
    date: "{today}（Today)",
    desc: "@:deliveryCartPanel.desc",
    time: "Delivery time",
    order: "Please select your meat",
    closed: "Currently not available",
    unavailable: "Delivery service opens from 11:00 of the day."
  },
  preorderRecord: {
    pickupTime: "Takeout time",
    status: "Status",
    number: "Order number",
    pickupButton: "Pick-up",
    pickupDesc1:
      "Your meal is not ready yet. Once it is ready, we will inform you through SMS.",
    pickupDesc2:
      "Please pick-up your meal within 20 minutes to the takeout time, in order to keep the meal to be at the best quality.",
    orderDetail: "Order items",
    productNote: "Remark：",
    total: "Sum of {count} items",
    shippingFee: "Shipping fee",
    points: "Reward points",
    totalAmount: "Total price",
    note: "Note",
    createdTime: "Order time",
    customerName: "Name",
    customerPhone: "Phone number",
    customerEmail: "Email",
    invoiceNumber: "Receipt number",
    invoiceDonation: "Giveaway your receipt",
    invoiceAddress: "Receipt address",
    companyName: "Title",
    taxIdNumber: "VAT number",
    cancel: "Cancel order",
    contact: "Customer service",
    back: "Back to Takeout status",
    desc:
      "We will inform you via SMS when your meal is ready. After the restaurant accepts your order, you cannot cancel the order. Please contact customer service if any requirement.",
    deliveryAddress: "Delivery address"
  },
  preorderGroup: {
    formatedPickupTimeToday: "M/D dd [(Today)]",
    formatedPickupTime: "M/D dd",
    pickDate: "Date",
    pickTime: "Time",
    estimatedPickupTime: "Takeout time",
    warningTag: "Warning",
    warningContent:
      "*Please select the pick-up time and date. Pick-up time: 12:00 noon to 8:00 PM. Some of the restaurants are not available for pick-up during afternoon break time."
  },
  errorCodes: {
    // Databases
    100000: "Database error",
    100001: "Failed to fetch data from database",
    100002: "Database contains dirty data",
    // Companies
    200001: "Failed to get reservation limit",
    // Reservations
    300001: "You're in the waiting list already. Please try again later.",
    300002: "No capacity available",
    // Branches
    400001: "No capacity settings found for this branch",
    // ValueError
    500000: "Inappropriate value",
    500001: "Invalid phone number",
    500002: "The group size you picked is out of available range"
  },
  preorderTutorial: {
    takeoutSteps: "外帶外送五步驟",
    pageTitle: "外帶外送說明",
    breadcrumb: "外帶外送說明",
    entranceText: "外帶外送說明",
    needHelps: "需要幫助？",
    needHelp: "需要幫助嗎？",
    details: "詳細說明",
    faq: "常見問題",
    button: {
      step1: "登入會員",
      step2: "選擇「您要的服務」",
      step3: "選擇餐廳與餐點",
      step4: "線上結帳",
      step5: "完成取餐享用餐點"
    },
    detailItem: {
      login: {
        header: "登入會員",
        step1: {
          title: "註冊與登入",
          text:
            '使用預訂外帶/外送服務必須進行會員登入。請點選右上角的<span class="anchor" hash="{signIn}">貴賓卡會員登入</span>，請直接輸入帳號密碼登入。<br />尚未註冊過的貴賓請點選<a class="anchor" target="_blank" href="{signUp}">加入美食地圖會員</a>並填寫個人資料與詳閱會員權益及使用約定條款後，即可享有新光三越美食地圖服務。'
        }
      },
      selectBranch: {
        header: "輸入查詢的店或輸入地址",
        step1: {
          title: "",
          text:
            '於<a class="anchor" href="{home}">首頁</a>或分店頁內選擇「您想前往的分店」或於「查詢符合美食外送的分店」輸入您的地址查詢。'
        }
      },
      selectRestaurant: {
        header: "選擇餐廳與餐點",
        service: "服務項目",
        step1: {
          title: "選擇餐廳與服務",
          text:
            "於分店頁選擇餐廳與服務，若選擇外送服務，請再次確認您的外送地址。"
        },
        step2: {
          title: "點餐",
          text:
            "點擊餐點可選擇餐點細節或註明餐點特殊需求，選擇份數後新增至購物車。"
        },
        step3: {
          title: "查看購物車",
          text:
            "將餐點放入購物車後，可由右下角購物車清單按鈕查看外帶細項，確認預訂內容。"
        },
        step4: {
          title: "購物車結帳",
          text: "選擇時間並確認訂單無誤，即可點擊下方按鈕前往結帳。"
        }
      },
      checkout: {
        header: "結帳",
        step1: {
          title: "選擇付款方式",
          text:
            "選擇付款方式，目前提供skm pay與信用卡線上刷卡全額付清服務，更多詳情請查看關於付款方式。"
        },
        step2: {
          title: "填寫訂購人與發票資訊",
          text:
            '填寫訂購人姓名、電話、Email 以及發票載具。<br>訂購人資料皆預設為您申辦會員時之資料，如當次訂購修改訂購人資料，將不影響會員資料，若有需調整會員資料，煩請至<a class="anchor" target="_blank" href="{joinMember}">新光三越官網貴賓卡會員中心</a>或skm app會員專區進行修改。'
        },
        step3: {
          title: "付款",
          text:
            "若選擇skm pay，系統將引導您開啟skm app進行付款。<br>若選擇信用卡，請輸入您的信用卡號並完成付款，付款成功後訂單將會送至餐廳，餐廳將於您預訂時間前開始為您準備餐點。為保障您可準時取餐，請儘速完成付款。"
        },
        step4: {
          title: "訂購完成",
          text:
            "訂單完成後，可點選前往檢視完整訂單，同時系統會寄發一封已完成訂購通知函與電子發票通知信到您的聯絡用E-mail信箱。<br />餐點準備完成後，會再另外寄簡訊通知您，如需更改或取消訂單，請至檢視完整訂單頁面店點選「聯絡客服」。"
        }
      },
      takeMeal: {
        header: "查詢訂單與取餐",
        step1: {
          title: "查詢訂單記錄",
          text:
            "點選右上方會員資訊，點選外帶外送紀錄可前往查看您所有的過往訂單。"
        },
        step2: {
          title: "外帶外送紀錄",
          text:
            "可以在此查看您所有的新進訂單與歷史紀錄。<br />確認訂單狀態與取餐時間或是檢視完整訂單，您也可以在這裡取消訂單。"
        },
        step3: {
          title: "取餐",
          text:
            "外帶-當外帶訂單狀態顯示「可前來取餐」即可前往餐廳，出示訂單記錄點選「我要取餐」，請出示 QR code 頁面給餐廳人員掃描來確認取餐，或者出示訂單編號來提供核銷。<br>外送-當外送訂單狀態顯示外送中，即代表外送員已出發至您的所在地，取餐時，請提供您的訂單編號，以供外送員核對，並請您簽名完成本次取餐。<br><br>*為響應環保，建議您可自備購物袋與餐具，一起愛護我們的地球。"
        }
      }
    },
    faqItem: {
      terms: {
        header: "條款宣告與平台權利",
        note1: {
          title: "條款宣告",
          text: `本服務條款（下稱「本條款」）約定您對新光三越百貨股份有限公司（下稱為「本公司」）所提供新光三越的美食地圖skm eats（以下統稱為「本平台」）的功能。使用本平台前請詳細閱讀本條款及本公司不定期調整之內容，如您開始使用任何功能，即表示您同意及接受本條款及修改內容。如果您不同意或不願遵守本條款，請勿使用本平台之任何功能。
          <br>本平台僅供年滿 20 歲的自然人和法人使用。未滿 20 歲的者，須先徵得法定代理人的同意，且法定代理人同意接受本條款，並對您的行為承擔全部責任，且全額支付您使用本平台購買餐點產生的任何費用。若您尚未徵得父母或法定代理人的同意，請勿使用本平台之任何功能。若您已使用，則視為已徵得父母或法定代理人的同意。
          <br>如本條款中的任何規定被法律認定為無效、違法或不可執行，則該類規定不影響本條款其餘規定，其餘規定應視為有效。本公司本條款、訂單及新光三越APP中之條款，構成了您與本公司之間的完整協議。
          <br>本條款依中華民國法律為準據法。如有爭議，以臺灣臺北地方法院為第一審管轄法院。
          <br>非餐點類之外送亦適用本條款相關付款及運送規範。
          <br>本公司將會依據本公司新光三越隱私權政策處理您的個人資料，該隱私政策構成本條款的一部分。您同意本公司及其他相關連的第三方，根據本條款與相關隱私政策中進一步詳細說明的規定，收集、使用、處理您的個人資料。`
        },
        note2: {
          title: "平台權利",
          text: `
          您同意本平台上的內容是在「既有」和「現有」的基礎上提供，您自行決定使用本平台以及透過本平台獲得的任何內容、商品、產品或服務，由您自行承擔相應風險。本公司會盡力確保可以提供本平台以及提供的服務，但本公司不擔保亦不聲明在提供服務時，始終安全、即時、不間斷、無錯誤、無技術困難、缺陷或系統中毒。且本平台可能會因定期或日常維護，以及因網路、電子通訊或不可抗力事件造成的停止運作，而暫時中斷。
          <br>本公司不承擔因本公司的網頁而造成的任何延誤、故障、錯誤、遺漏或丟失傳送的資料，轉發的病毒或其他汙染，破壞屬性的任何責任。
          <br>本公司不為任何店家的延誤、疏忽，與因不可抗力因素影響造成的延遲或疏忽負責。您於本平台上執行之操作均由系統即時接收，並依相關服務指令即時或批次進行處理，除本公司另有明定外，交易經確認後，即不得撤回、取消或修改。
          <br>本公司已採取了可以合理防止網路詐騙的方式，並確保您私人資料的安全。但不因重大事件﹝如因電腦伺服器的受損或破壞、第三方的違約﹞而負責。
          <br>一旦發現有濫用優惠券或其他有詐騙之虞時，本公司可以立即停止並拒絕您使用本平台，且得向您請求賠償。
          <br>您同意如因您不當使用本平台或您違反本條款而致的任何第三方賠償責任，並不會向本公司任何求償。

          `
        },
        note3: {
          title: "第三方連結、網站與法律",
          text: `
          本平台可能包含其他第三方網站的連結，點按這些連結，即視為您同意自行承擔轉至這些網站的風險。
          <br>本公司對於第三方網站或連結之內容不負任何責任。如您因使用這些第三方連結和網站，而受有損害，本公司並不對此負任何責任。
          <br>請勿在本公司本平台上使用任何自動化或外掛程式來獲取本平台的程式。
          <br>請勿從本公司本平台收集或尋求任何可識別的個人資訊、使用本平台提供的系統做任何商業招攬之目的之事項、以任何理由招攬本平台的使用者、未經本公司同意自行發佈或發放任何折價卷或網站連接的代碼、破壞或破解本平台。
          <br>如果本公司合理懷疑或發現您使用本台平有欺詐行為、違反善良風俗或任何違反條款的行為，本公司有權取消您的任何訂單與服務或暫停停用、刪除或終止您的帳戶和存取平台的權限。

          `
        }
      },
      meal: {
        header: "關於餐點",
        note1: {
          title: "過敏原及食用期限說明",
          text: `
          本公司不保證店家出售的商品不含特定過敏原。店家在準備特定餐點時，可能會使用對您有過敏之食材。若您對特定食物過敏，請在訂購前先行了解，並於訂購時於訂單中特別標註或於訂購前先與店家聯繫。
          <br>在領取餐點後，請於店家所建議的最佳食用時間或盡速將餐點食用完畢，以確保餐點品質。
          `
        },
        note2: {
          title: "確認餐點",
          text: `當外送員將您所訂購之餐點交付給您時，請先檢查餐點之外觀、品項、數量等是否正確，如有錯誤請立即透過電話或來信通知本公司。`
        },
        note3: {
          text:
            "若須登打公司統一編號，請務必於結帳時點選【電子發票證明聯紙本】，並輸入公司統一編號及公司名稱，前往您購買的店別之卡友服務中心出示您該筆訂單的資訊索取紙本發票。"
        }
      },
      order: {
        header: "訂單相關",
        note1: {
          title: "訂單的成立與支付",
          text: `
          您透過本平台下單後，本公司會向您發送一封包含訂單資訊的電子郵件，以確認您的訂單，電子郵件內容包括餐點費用、運送費用及相關稅額（如有）。店家於接受您的訂單後開始準備餐點，您於訂購時須載明運送地點與聯繫方式。
          <br>餐點及運送費用支付方式可使用信用卡與skm pay全額付清，並可使用國民旅遊卡消費及補助核銷。本公司接受之信用卡包括VISA、MasterCard、JCB等三家國際發卡組織之信用卡。請於使用時務必確實填寫信用卡資料；若發現有不實登錄或任何未經持卡人許可而盜刷其信用卡的情形時，相關責任由您承擔。本公司有權暫停或終止您的權益，並行使相關權利。
          <br>信用卡線上交易授權必須透過本公司與您的發卡銀行聯繫，倘若發生任何無法完成線上交易授權的原因，相關的訊息由於牽涉個人資料，建議您可自行聯繫發卡銀行了解原因。
          <br>如出現無法正常結帳情形，可能係因：
          <br>(1)網路不穩、同時間訂購人數較多，建議您稍等一段時間再重新訂購商品。
          <br>(2)信用卡刷卡額度不足造成之刷卡失敗，由於本平台交易皆為線上進行，為確保持卡人的資料安全，請您親自向發卡銀行確認。
          <br>【※如您使用非本人信用卡所產生爭議，由您自行承擔，如造成本公司受有損害，並應負損害賠償責任。】
          `
        },
        note2: {
          title: "費用",
          text: `
          本平台所有價格均以新台幣計算，並皆已含稅。本公司在可能在您流覽餐點時調整餐點及遞送費用，但此調整不會影響已確認之訂單。如餐點價格有錯誤，本公司將會立即通知您，您得選擇是否以正確價格訂購，或不需支付任何費用取消訂單，並全額退款。
          <br>您訂單應支付的全部價格將於本平台的結帳頁面顯示，包含餐點費用、運送費用與稅額（如有）。
          <br>本公司店家本平台響應電子發票政策，全面採用雲端發票，電子發票證明聯不再主動寄出。本平台消費所開立之「雲端發票」將於發票開立後48小時內上傳至財政部電子發票整合服務平台留存，您可於該平台查詢發票資訊，亦可於本網站訂單查詢頁面查詢發票號碼與交易明細。
          <br>本平台將每期自動幫您兌獎雲端發票，若您幸運中獎將以E-mail方式或簡訊寄發中獎通知，更多訊息，請至新光三越電子發票使用說明(https://www.skm.com.tw/w/sk?a=ilanztu)了解。若您須登打公司統一編號，請務必於結帳時點選【電子發票證明聯紙本】，並輸入公司統一編號及公司名稱，前往您購買的店別之卡友服務中心或店家出示您該筆訂單的資訊索取紙本發票。
          `
        },
        note3: {
          title: "資料正確性(收件人、連絡方式、地點、時間等",
          text: `
          您透過本平台下單，請在點按「結帳」後依指示進行操作，您同意並確認您提供的所有資訊（包括金額、運送地點、連繫方式、付款資訊和優惠碼（如有））均屬正確，同時保證您於訂購時所提供的信用卡為您本人所有或已獲得信用卡持卡人同意使用【※如您使用非本人信用卡所產生爭議，由您自行承擔，如造成本公司受有損害，並應負損害賠償責任。】，如果您收到包含您訂單資訊的確認電子郵件，即表示您已成功下單。店家將負責準備您的餐點，並交由本公司委託對象運送。若因餐點內容錯誤、傾覆等需要求退款，請您撥打新光三越免付費客服專線。若須退款時，您亦保證您所提供之帳戶資訊為您本人所有且正確無誤。
          <br>餐點將會運送到您於訂單中填寫的運送地點，您必須確保您所填寫的地址正確無誤，並且有人收受餐點，如果您所填寫之地址不正確，本公司將無法運送餐點給您，您仍需承擔此訂單之費用。本公司不負責審查運送地點領取餐點之人（包括但不限於訂購之本人、其親友、管理員或其他人），是否為您本人或者是否有權收受餐點。餐點提供予領取人時，所有風險與責任將同時轉移給您。
          <br>本公司在您提交訂單後，如果要更改運送地點，您必須提前與本公司聯繫。如本公司無法更改運送地點，您可選擇取消訂單，但如店家已接受訂單，您仍須支付餐點費用，且如外送員已至店家領取餐點，您亦須支付運送費用。
          <br>本公司如因下列原因造成餐點運送失敗，您仍須支付餐點及運送費用：
          <br>(1)無人領取餐點。
          <br>(2)外送員以您提供之聯絡資訊聯繫後，仍無法與您取得聯繫。
          <br>(3)無適當的途徑交付餐點。
          <br>(4)缺乏合適地點來放置餐點。
          `
        }
      },
      takeMeal: {
        header: "關於取餐",
        note1: {
          title: "外帶自取",
          text: `
          您可以選擇直接從店家外帶自取（下稱「自取」），並可至【預訂外帶紀錄】確認訂單狀態，在店家確認以前，您可自行點選【取消訂單】申請訂單取消。若無法線上成功取消訂單，表示店家已確認您的餐點，則無法取消您的訂單。若您是有預定取餐時間，在未取餐時如希望取消訂單，請您於預訂取餐時間至少一小時前選取【聯絡客服】通知本公司，將由客服人員為您查詢處理。您應確認電子郵件內註明您自行取貨的時間（下稱「取貨時間」）。店家會在取貨時間之前準備訂單。惟仍可能發生合理的延遲。
          <br>如果由於您的個人因素而造成延遲自取，您應承擔任何餐點毀損或餐點品質變化等風險。於此情況下，您無權要求換貨或退貨。在自取時自行檢查餐點，如您已離開店家，餐點之任何問題，本公司或店家並不負任何責任。
          <br>當確認完成取消訂單後，系統將發出訂單取消通知信，此訂單款項將於7~10個工作天內進行退款作業，退貨款項將退至您原付款之信用卡帳戶。實際入帳日期將視您的發卡銀行作業時間而定。
          `
        },
        note2: {
          title: "送餐地點、時間的彈性",
          text: `
          本平台所提供的交貨時間僅為預估時間，餐點之實際運送時間可能會因當時之天候與交通狀況等不可抗力之事由或店家之準備餐點進度而出現差異。您不得因餐點遲延而要求取消訂單及退款，或要求本公司或店家賠償或補償任何費用。
          <br>本平台所有的訂單將由合作單位負責指派外送員運送。本公司會盡力要求合作單位於交貨時間內送達餐點本公司。
          <br>如因不可抗力，致外送員無法將餐點運送到您填寫之運送地址時，本公司將以您提供之聯絡方式通知您，並提供您取消訂單或是改變外送地址等服務。
          <br>本公司在本平台上輸入您的運送地點，您就能知悉得向您提供服務的店家。惟可能因天氣和交通條件等不可抗力因素，服務範圍可能會變動。
          `
        },
        note3: {
          title: "收件人與代收人",
          text: `
          當外送員將餐點送至您填寫之運送地點後，將通知您取餐，基本上本公司會於運送地點等待十分鐘，惟如仍無人前來領取餐點或無法聯繫到您時，外送員將視情況將餐點放置在合適地點，但此非外送員之義務，您不得以外送員未留置而要求賠償。若於時間內皆無獲得您回應且無合適地點放置餐點，外送員將離開運送地點，並視同您放棄該餐點，本公司無需退款或採取其他補救措施。
          `
        }
      },
      cancelOrder: {
        header: "取消訂單與退貨",
        note1: {
          title: "訂單的取消",
          text: `
          您可至【預訂外帶/外送記錄】確認訂單狀態，在店家尚未接單之前，您可自行點選【取消訂單】申請訂單取消，並獲取訂單的退款。若無法線上成功取消訂單，表示店家已確認接單、餐點準備中或外送員已領取餐點，在這之後如果您仍決定取消訂單，即表示您瞭解店家不會向您提供退款，本公司也不會運送已取消訂單的餐點。若您是有預定取餐時間，在未取餐時希望取消訂單，請您於預訂取餐時間至少一小時前選取【聯絡客服】通知本公司，將由客服人員為您查詢處理。
          <br>當因任何原因導致店家不供應您所訂購之餐點或產品或外送員無法送餐或您因其他不可抗力原因而欲取消您的訂單，且本公司亦取得店家同意時，本公司將會視情況取消訂單，並退還您所支付之費用，其中包括運費或其他服務費用，但不包含訂單所使用的優惠或折價券。
          <br>當需退款時，此款項將於7~10個工作天內進行退款作業，退貨款項將退至您原付款之信用卡帳戶。（實際入帳日期將視您的發卡銀行作業時間而定）
          `
        },
        note2: {
          title: "訂單錯誤、商品缺失、商品缺陷",
          text: `
          收到訂單餐點或商品後，如果您發現有問題時（如：內容錯誤或餐點毀損等），請立即聯繫客服。本公司可能會要求您拍照或提供其他資訊，以查明店家或外送員是否提供錯誤的餐點或餐點毀損情形，您可以拒絕接受此餐點，本公司將會全額退還餐點費用。但如果是您有特殊餐點備註，本公司和店家將盡力配合，然如果您的特殊備註是不可行或不合理而未完全符合您的需求時，店家得依標準程序為您製作餐點，此情形將不屬於退款範疇。
          `
        },
        note3: {
          title: "無法退貨原則之聲明",
          text: `
          本平台提供之餐點因屬於易於腐敗、保存期限較短或退貨時即將逾期之商品（如現做的餐點食物、蛋糕、麵包、生鮮食品等），除本條款另有載明之外，恕無法接受無條件退貨之要求。
          `
        }
      },
      other: {
        header: "其他",
        note1: {
          title: "優惠券",
          text: `本公司可能會不定時舉辦優惠、折扣等行銷和促銷活動。優惠券具有使用期限，使用次數以優惠券表示為準。優惠券不可與其他促銷活動、折扣或其他優惠一併使用。優惠券須遵守相關條件。除另有說明外，優惠券只能在本平台上使用。優惠券不能兌換成現金。本公司保留取消、停止或拒收任何優惠券的權利，恕不另行通知。店家本公司隨時新增或禁止某些店家使用優惠券，而不必事先通知您。
          `
        }
      }
    }
  },
  realMap: {
    expandMap: "Show Full Map",
    collapseMap: "Close Full Map",
    dragHint: "Drag to Check Full Map"
  },
  remind: "Hint"
};
