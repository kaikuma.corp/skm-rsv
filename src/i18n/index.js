import Vue from "vue";
import VueI18n from "vue-i18n";
import moment from "moment";
import store from "../store";
import messages from "./messages";
import "moment/locale/zh-tw";

Vue.use(VueI18n);

moment.locale(store.state.locale.toLowerCase());

const htmlLangMappings = {
  "zh-TW": "zh-Hant-TW",
  "en-US": "en"
};

const html = document.documentElement;

const i18n = new VueI18n({
  locale: store.state.locale,
  silentTranslationWarn: true,
  messages
});

const setLanguage = locale => {
  html.lang = htmlLangMappings[locale];
  document.title = i18n.t("site-name");
};

export const changeLocale = locale => {
  moment.locale(locale.toLowerCase());
  i18n.locale = locale;
  setLanguage(locale);
};

setLanguage(store.state.locale);

export const API_LOCALE = {
  "zh-TW": "ZH",
  "en-US": "EN"
};

export default i18n;
