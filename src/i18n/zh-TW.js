const landing = {
  title: "新光三越美食地圖",
  subTitle: "為您推薦美食餐廳，提供訂候位/外帶/外送服務。",
  available: {
    title: "目前開放分店：",
    delimeter: "、"
  },
  loadingMask: {
    locating: "定位中"
  },
  button: {
    booking: "我要訂餐廳",
    preordering: "我要預訂外帶"
  }
};

const branchSelector = {
  or: "或",
  title: "您想前往享用美食的分店",
  pleaseSelect: "請選擇分店",
  select: "查詢您附近的分店",
  noResult: "您的地址不在本店外送範圍，請更改您的外送地址",
  submit: "進入美食地圖",
  unavailable: "(未開放)",
  searchSelector: {
    title: "查詢符合美食外送的分店",
    deliverTitle: "您想美食外送的地址",
    resultTitle: "選擇您喜愛的分店美食外送",
    resultTitle2: "您的地址符合本店外送範圍",
    choose: "您的地址符合本店外送範圍",
    chooseTime: "請選擇時間",
    noResult:
      "您的所在地址不在外送服務範圍，推薦您選擇所在地鄰近分店使用訂候位及外帶服務。",
    noResult2: "{recommend}",
    resultHasRecommend:
      "您的所在地址不在外送服務範圍，推薦您選擇所在地鄰近分店使用訂候位及外帶服務。",
    resultNoRecommend:
      "您的所在地址不在本店外送服務範圍，請重新輸入地址或選擇其他美食服務。",
    otherService: "您可以選擇其他服務"
  },
  button: {
    goNow: "立即前往",
    checkBranch: "查詢分店",
    back: "返回",
    back2: "變更",
    confirm: "確定",
    goDelivery: "前往訂餐"
  }
};

const placeholder = {
  select: "請選擇",
  selectCity: "請選擇縣市",
  selectArea: "請選擇區域",
  selectBranch: "請選擇分店",
  input: "請輸入{postfix}",
  address: "請輸入地址"
};

export default {
  placeholder,

  landing,
  branchSelector,

  records: {
    pageTitle: "會員訂候位紀錄",
    noBookingRecords: "尚未有訂位紀錄",
    noWaitingRecords: "尚未有候位紀錄",
    count: "總共 {n} 筆",
    breadcrumb: "訂候位紀錄查詢",
    waiting: {
      tab: "候位記錄 ({n})"
    },
    booking: {
      tab: "訂位記錄 ({n})"
    },
    walkIn: {
      tab: "現場 ({n})"
    },
    groupSize: "{adultCount} 大 {childrenCount} 小",
    restaurantButton: "餐廳資訊",
    reservationButton: "訂候位資訊",
    desktopTable: {
      time: "登記時間",
      name: "餐廳名稱",
      state: "訂候位狀態",
      seatedTime: "入座時間",
      groupSize: "用餐人數",
      note: "備註",
      link: "查看訂候位資訊"
    }
  },

  reservation: {
    state: {
      booking: {
        waiting: "未入座",
        cancelled: "已取消",
        seated: "已入座"
      },
      waiting: {
        waiting: "候位中",
        cancelled: "已取消",
        seated: "已入座"
      },
      "walk-in": {
        waiting: "候位中",
        cancelled: "已取消",
        seated: "已入座"
      }
    },
    noCustomerNote: "無"
  },

  account: {
    hi: "親愛的 {level} {name} 您好：",
    levelTitle: {
      N: "貴賓卡貴賓",
      S: "銀級貴賓",
      V: "尊榮級貴賓",
      "0": "會員"
    },
    menu: {
      logout: "登出",
      reservationRecords: "訂候位紀錄查詢",
      preorder: "外帶外送記錄",
      cardDetail: "{level}禮遇"
    },
    gender: {
      male: "先生",
      female: "小姐"
    },
    vipTreatment: {
      treatment1: {
        title: "禮遇 1",
        content: "享部分餐廳訂位保留席"
      },
      treatment2: {
        title: "禮遇 2",
        content: "享優先候位<br />更快地入座用餐"
      }
    }
  },

  navMenu: {
    title: "美食地圖",
    skmHomePage: "新光三越官網"
  },

  "site-name": "新光三越美食訂候位服務",
  nav: {
    languages: {
      "zh-TW": "繁中",
      "en-US": "English",
      mb: {
        "zh-TW": "繁",
        "en-US": "EN"
      }
    },
    login: {
      desktopButton: "貴賓卡會員登入",
      mobileButton: "登入"
    }
  },
  header: {
    menu: {
      discovery: "我要訂餐廳",
      preorder: "我要預訂外帶"
    },
    button: {
      "goto-nanxifoodmap": "前往南西商圈",
      "goto-skm-nanxi": "前往台北南西店"
    }
  },
  group: {
    nanxifoodmap: "南西商圈",
    kengroup: "台北信義新天地",
    "skm-xinyi": "台北信義新天地",
    "skm-taichung": "台中中港店",
    "skm-nanxi": "台北南西店",
    "skm-tainanplace": "台南新天地",
    "skm-TaipeiStation": "台北站前店",
    "skm-TaipeiTianmu": "台北天母店",
    "skm-KaohsiungZuoying": "高雄左營店",
    "skm-KaohsiungSanduo": "高雄三多店",
    "skm-TainanZhongshan": "台南中山店",
    "skm-TaoyuanStation": "桃園站前店",
    "skm-TaoyuanDayou": "桃園大有店",
    "skm-ChiayiChuiyang": "嘉義垂楊店"
  },
  groupDescription: {
    nanxifoodmap:
      "位於捷運中山站，結合獨特的巷弄文化與人文藝術，精選隱藏版餐廳，提供三五好友相聚的用餐環境。",
    kengroup:
      "位於台北都會精華-信義計畫區，A4、A8、A9、A11共有 38 家精選餐廳任您挑選。",
    "skm-xinyi":
      "位於台北都會精華-信義計畫區，A4、A8、A9、A11精選 46 家餐廳、咖啡廳任您挑選。",
    "skm-taichung":
      "臺灣中部最完整舒適的購物娛樂商場，主題美食街分層設立，9F、10F搜羅所有好評料理餐廳，13F提供三五好友一起宵夜放鬆享受的美式料理。",
    "skm-nanxi":
      "位於台北流行發信地的中山商圈，提供台灣道地美食小吃與台北最熱門人氣餐廳。",
    "skm-tainanplace":
      "南台灣首家大型百貨商場，精選最夯美食餐廳，滿足每位饕客全日用餐選擇。",
    "skm-TaipeiStation":
      "位在五鐵共構的交通樞紐，是台北西區重要的地標景點， B1、11F、12F餐廳精選各類型料理，提供旅客或商務聚餐最方便快速的選擇。",
    "skm-TaipeiTianmu":
      "位於台北最有異國情調的天母商圈中， A、B兩館提供各類型的異國美食，不論味覺或視覺，都讓人彷彿置身國外。",
    "skm-KaohsiungZuoying":
      "位居高雄高鐵、台鐵、捷運三鐵共構轉運站，精選最夯美食餐廳，提供每位旅客全日用餐選擇。",
    "skm-KaohsiungSanduo":
      "位於南高雄最熱鬧的三多商圈，適合全家大小逛街購物、享受美食的最佳好地點。",
    "skm-TainanZhongshan":
      "台南火車站前最流行的百貨公司，12F精選最夯美食餐廳，獨享、聚餐都滿足。",
    "skm-ChiayiChuiyang":
      "座落於嘉義市人潮鼎沸的垂楊商圈，精選美食餐廳，提供給家庭朋友最佳聚餐地點。",
    "skm-TaoyuanDayou":
      "位於桃園大有特區之內，鄰近南崁及臺灣桃園國際機場，是享受美食的最佳好地點。",
    "skm-TaoyuanStation":
      "位於桃園火車站前，桃園商業中心內，精選最夯美食餐廳，提供旅客或商務聚餐最方便快速的選擇。"
  },
  breadcrumb: {
    root: "美食地圖",
    preorderRecords: "外帶外送紀錄",
    takeout: "外帶",
    reservation: "訂候位",
    waitingStatus: "等候狀況"
  },
  building: {
    restaurantList: "{buildingName} 所有餐廳列表",
    floor: "{floor} 樓",
    restaurantsCount: "{count} 家餐廳",
    "skm-xinyi": {
      building121: "",
      building122: "",
      building123: "",
      building124: ""
    },
    "skm-taichung": {
      main: "台中中港店",
      building201: ""
    },
    "skm-nanxi": {
      building101: "一館",
      building102: "二館",
      building103: "三館"
    },
    "skm-tainanplace": {
      building321: "本館",
      building322: "小西門"
    },
    "skm-TaipeiTianmu": {
      building151: "A館",
      building152: "B館"
    },
    "skm-TaipeiStation": {
      building111: "台北站前店"
    },
    "skm-KaohsiungSanduo": {
      building301: "高雄三多店"
    },
    "skm-TainanZhongshan": {
      building311: "台南中山店"
    },
    "skm-KaohsiungZuoying": {
      building351: "高雄左營店",
      building352: "彩虹市集"
    },
    "skm-ChiayiChuiyang": {
      building331: "嘉義垂楊店"
    },
    "skm-TaoyuanDayou": {
      building161: "桃園大有店"
    },
    "skm-TaoyuanStation": {
      building171: "桃園站前店"
    }
  },
  restaurant: {
    tab: {
      booking: "訂位",
      waiting: "候位",
      preorder: "外帶",
      delivery: "外送"
    },
    title: {
      waiting: "餐廳候位狀況",
      booking: "餐廳訂位",
      preorder: "外帶",
      delivery: "外送"
    },
    orderTitle: "請選擇餐點",
    bookingAnnouncement: "最新消息",
    readMore: "查看更多",
    readLess: "隱藏",
    preorderButton: "我要點餐",
    today: "{date}（今天）",
    waitingLine: "現在排隊情況：",
    info: "餐廳資訊",
    location: "餐廳位置",
    type: "料理類型",
    minimumCharge: "每人低消",
    phone: "店家電話",
    openingTime: "營業時間",
    acceptablePaymentOption: "可接受付款方式",
    acceptablePaymentOptions:
      "現金、skm pay、禮券(禮券、商品禮券、電子券類、餘代卡)、信用卡 (VISA、 MASTERCARD、 JCB、 AE、銀聯卡、晶片金融卡、Apple Pay 、Android Pay、Samsung Pay )、LINE Pay  、微信支付、支付寶",
    menu: "菜單",
    waitingAndBooking: "餐廳訂候位狀況",
    recommendedRestaurants: "其他推薦餐廳",
    waitingTime: "等待 {minutes} 分鐘",
    waitingTime2: "等候約 {minutes} 分",
    longWaiting: "等待長時間",
    waitingCount: "目前排隊組數",
    waitingCountDesc: "{waitingCount}  組候位中",
    iWantToWait: "我要候位",
    iWantToWaitVip: "我要候位",
    goToPickNumber: "請前往餐廳排隊取號",
    notAllowBooking: "餐廳未開放<br>線上訂位",
    waitingDisclaimer: "實際等候時間及排隊人數，依各餐廳現場為主",
    iDontWantToWait: "想立馬用餐但又不想等太久嗎？",
    noWaiting: "目前尚有空位",
    noWaitingDesc: "可以前往餐廳直接入座用餐",
    waitingStatusClosed: "已關閉候位",
    closed: "已打烊",
    temporarilyClosed: "餐前準備中，請選擇其他餐期",
    waitingTemporarilyClosedLine1: "餐前準備中",
    waitingTemporarilyClosedLine2: "請選擇其他餐期",
    notOpen: "尚未營業",
    closedDesc: "{nextOpenTimeFrom} 開始營業",
    dayOff: "今日不營業",
    noNextOpenTime: "本日已結束營業",
    waitingStatus: {
      closed: "請電洽詢問或至現場候位",
      nowAvailable: "目前有空位可前往"
    },
    noSlotAtSession: "線上訂位已額滿，請選擇其他餐期",
    bookingDisabled: "尚未開放訂位，請電洽或使用候位",
    floor: "{floor}樓",
    preorderClosed: "已關閉預訂",
    preorderUnavailable: "選擇的時段無法預訂",
    preorderNotStarted: "請選擇時間",
    preorderDesc: "@:restaurantPreorderTimeSelector.desc",
    deliveryDesc: "@:restaurantDeliveryTimeSelector.desc"
  },
  date: {
    today: "今日"
  },
  list: {
    close: "暫未開放線上候位",
    unavailable: "已無訂位的餐廳",
    disabledPreorder: "目前關閉預訂"
  },
  filters: {
    numOfPersons: "用餐人數",
    personUnit: "位",
    date: "用餐時間",
    strategies: {
      waiting: "等候狀況",
      bookingNoon: "訂中午",
      bookingAfternoon: "訂下午",
      bookingNight: "訂晚上"
    }
  },
  restaurantTypes: {
    all: "全部料理",
    chinese: "中式",
    japanese: "日式",
    korean: "韓式",
    american: "美式",
    vegi: "蔬食",
    southeastasia: "東南亞",
    italian: "義式",
    hk: "港式",
    bbq: "燒烤",
    cafe: "咖啡廳",
    teppanyaki: "鐵板燒",
    hotpot: "火鍋",
    steak: "牛排",
    allucaneat: "吃到飽",
    family: "親子餐廳",
    mall: "百貨商圈",
    undefined: "未提供"
  },
  actions: {
    book: "我要訂位",
    back: "上一頁",
    submit: "確認送出",
    next: "下一步",
    viewReservation: "檢視訂位資訊",
    backToFoodMap: "回美食地圖首頁"
  },
  sorry: {
    noSlot: "此時段無提供訂位，請參考其他時段",
    noSlotAllDay: "選擇的日期無提供任何時段訂位"
  },
  bookingForm: {
    title: "填寫訂位資訊",
    info: "訂位資訊",
    numOfPersons: "@:filters.numOfPersons",
    date: "@:filters.date",
    time: "用餐時段",
    name: "訂位人姓名",
    namePlaceholder: "王小明",
    phone: "訂位人電話",
    phonePlaceholder: "0912345678",
    phoneHint: "（訂位成功會寄送簡訊通知）",
    purpose: "用餐目的",
    sizeLimitHint: "線上訂位最多可至 {size} 位，如需團體訂位請撥打餐廳電話",
    nAdult: "{n} 位大人",
    nKid: "{n} 位小孩",
    nAdultNKid: "{adult} 大 {kid} 小",
    gender: {
      mr: "先生",
      miss: "小姐"
    },
    purposes: {
      celebration: "慶生",
      dating: "約會",
      annualSale: "週年慶",
      family: "家庭用餐",
      friend: "朋友聚餐",
      business: "商務聚餐"
    },
    note: "訂位備註",
    notePlaceholder:
      "有任何特殊需求嗎？可以先寫在這裡喔！<例如：不吃牛、不吃蔥蒜、蛋奶素等>",
    additionalRequirement: "額外需求",
    additionalRequirements: {
      kidSet: "兒童椅安排",
      kidSetUnit: "張",
      pram: "嬰兒車停放",
      pramUnit: "台",
      hint: "上述需求將依餐廳現場安排為主"
    },
    result: {
      success: "訂位成功",
      failure: "訂位失敗",
      smsToPhone: "已發送簡訊至手機 {phone}"
    },
    customerInfo: "訂位人資訊",
    editContactInfo: "更改聯絡人資訊"
  },
  waitingForm: {
    waitingGroupSize: "登記候位用餐人數",
    nAdult: "{n} 位大人",
    nKid: "{n} 位小孩",
    maxGroupSize: "線上候位登記最多可至 {n} 位",
    contactInfo: "聯絡人資訊",
    contactInfoDesc: "（登記成功會寄送簡訊通知）",
    contactName: "登記人姓名",
    contactPhone: "登記人電話",
    editContactInfo: "更改聯絡人資訊",
    note: "候位備註",
    notePlaceholder:
      "有任何特殊需求嗎？可以先寫在這裡喔！（例如：不吃牛、不吃蔥蒜、蛋奶素等）",
    additionalRequirement: "額外需求",
    additionalRequirements: {
      kidSet: "兒童椅安排",
      kidSetUnit: "張",
      pram: "嬰兒車停放",
      pramUnit: "台",
      hint: "上述需求將依餐廳現場安排為主"
    },
    submit: "確認送出",
    back: "回上一頁",
    succeeded: {
      message1: "您已登記候位成功",
      message2:
        '目前排隊總組數為 <span class="count">{count}</span> 組，<br />由於您是我們的尊榮級貴賓，已為您優先安排座位。'
    },
    smsSent: "已發送簡訊至手機 {phone}",
    viewReservation: "檢視候位狀態",
    backToHome: "回美食地圖首頁",
    vipTreatment: {
      title: "尊榮禮遇",
      content: "登記即可優先候位"
    }
  },
  loginMessage: {
    general: {
      title: "嗨！親愛的{level}<br /><strong>{name}</strong> 您好",
      message:
        "歡迎來到新光三越美食地圖,{level}可享線上立即登記候位與24小時網路訂位服務",
      closeButton: "知道了"
    },
    vip: {
      title: "嗨！親愛的{level}<br /><strong>{name}</strong> 您好",
      message:
        "歡迎來到新光三越美食地圖，{level}可享線上立即登記候位與24小時網路訂位服務",
      closeButton: "知道了"
    }
  },
  loginBanner: {
    booking: {
      title: "登入會員，立即線上訂位",
      desc: "貴賓卡會員可享有餐廳線上訂候位服務，快登入新光三越貴賓卡會員吧！"
    },
    waiting: {
      title: "登入會員，立即線上候位",
      desc: "貴賓卡會員可享有餐廳線上訂候位服務，快登入新光三越貴賓卡會員吧！"
    }
  },
  groupTabs: {
    booking: "我要訂餐廳",
    preordering: "我要預訂外帶",
    map: "美食樓層導覽",
    map2: "美食地圖導覽"
  },
  preorderDateSelector: {
    cancel: "取消",
    confirm: "完成",
    title: "選擇時間"
  },
  preorderRecords: {
    cancelConfirm: "訂單一經取消後，則無法復原。您確認要取消此筆訂單？",
    cancelSuccess:
      "訂單取消完成！我們將於7-10個工作天內進行退款申請作業，款項將退至您原付款之信用卡帳戶，實際入帳日期將視您的發卡銀行或信用卡公司作業時間有所不同。",
    cancelError:
      "您的訂單已成立，若有需要其他服務請使用聯絡客服功能，我們盡快與您聯繫。",
    title: "外帶外送紀錄",
    newRecords: "新進訂單 ({count}筆)",
    historyRecords: "歷史紀錄 ({count}筆)",
    pickUpRecords: "外帶紀錄 ({count}筆)",
    deliverRecords: "外送紀錄 ({count}筆)",
    total: "共 {count} 筆"
  },
  preorderContact: {
    title: "聯絡客服",
    detailTitle: "你的訂單",
    restaurant: "外帶餐廳",
    time: "外帶時間",
    number: "訂單編號",
    status: "訂單狀態",
    cancelledDesc: "退款處理中 (餐廳取消)",
    email: "您的聯絡電子信箱(*)",
    emailPlaceholder: "Email",
    content: "內容(*)",
    contentPlaceholder: "詢問更改訂單、取消訂單或其他相關問題",
    desc:
      "歡迎詢問訂單的相關問題，送出後我們的客服會儘速聯繫您，請耐心等候，謝謝！",
    submit: "送出",
    successMessage:
      "歡迎詢問訂單的相關問題，送出後我們的客服會儘速聯繫您，請耐心等候，謝謝！",
    close: "關閉"
  },
  preorderQrCode: {
    number: "訂單編號",
    desc:
      "請出示 QR code 頁面給餐廳人員掃描來確認取餐，或者出示訂單編號來提供核銷。"
  },
  preorderRecordItem: {
    pickupTime: "{time} 取餐",
    deliveryTime: "{time} 送餐",
    number: "訂單編號 {number}",
    points: "點數折抵",
    cancel: "取消訂單",
    contact: "聯絡客服",
    view: "檢視完整訂單"
  },
  preorderRecordList: {
    orderTime: "點餐時間",
    restaurant: "餐廳名稱",
    pickupTime: "預計取餐時間",
    number: "訂單編號(末6碼)",
    amount: "訂單金額",
    status: "訂單狀態",
    action: "訂單動作",
    cancel: "取消訂單",
    contact: "聯絡客服",
    view: "檢視完整訂單",
    noRecords: "尚未有預訂紀錄"
  },
  preorderPickup: {
    title: "您已完成取餐",
    desc: "感謝您耐心等候，請享用您的外帶餐點！",
    back: "回到訂單紀錄"
  },
  preorderStatus: {
    1: "等待餐廳接單",
    2: "已取餐",
    3: "已取消",
    4: "可前來取餐",
    5: "餐點準備中",
    6: "已取消",
    7: "等待外送取餐",
    8: "送餐中",
    N: "訂單未完成",
    Y: "訂單完成",
    crossDate: "未到取餐日"
  },
  preorderCart: {
    title: "購物車清單",
    amount: "累計",
    count: "購物車清單共 {count} 項"
  },
  preorderCartPanel: {
    title: "我的購物車",
    restaurant: "外帶餐廳",
    pickupTime: "預計取餐時間",
    yourOrder: "您選擇的餐點",
    amount: "小計",
    note: "訂單備註",
    notePlaceholder: "在餐點上有什麼特殊需求，可以在這裡註記喔！",
    submit: "前往結帳",
    remove: "移除",
    productNote: "備註：{note}",
    timeSelectorDesc: "*取餐時間為您所預訂日期12:00-20:00",
    desc:
      "*為維持餐點品質為最新鮮狀態，以及餐點保存空間的乾淨衛生，請於預定取餐時間20分鐘內前往取餐。",
    total: "合計"
  },
  deliveryCartPanel: {
    restaurant: "外送餐廳",
    pickupTime: "預計送達時間",
    desc:
      "為維持餐點為最新鮮狀態，餐點皆現點現做。較繁忙時刻餐點送達時間可能會延遲15~20分鐘， 請耐心等待，謝謝",
    charge: "運費",
    timeSelectorDesc: "*送餐時間為您所預訂日期12:00-21:00"
  },
  preorderProductPanel: {
    note: "餐點特殊需求",
    notePlaceholder: "口味偏好或者過敏...",
    updateCart: "更新購物車",
    addToCart: "加入購物車",
    unavailableMessage: "11:00 以後開放當日預訂，非預訂開放時間僅可檢視餐點。",
    unavailableMessage2: "本產品目前已售完",
    limitedQty: "外帶最高上限 8 份",
    cartInCount: "，購物車內已有 {count} 份了唷"
  },
  deliveryProductPanel: {
    limitedQty: "您的訂單餐點已達配送量，請完成訂單後另啟新訂單。"
  },
  restaurantPreorder: {
    orderButton: "我要點餐",
    readOnlyOrderButton: "詳細餐點內容",
    unavailableProducts: "{products} 已經下架，請再次確認餐點。"
  },
  restaurantPreorderNav: {
    pickupTime: "選擇取餐時間",
    order: "選擇外帶餐點"
  },
  restaurantDeliveryNav: {
    pickupTime: "選擇送達時間",
    order: "選擇外送餐點"
  },
  cartPanel: {
    unavailableTimeMessage: "由於作業已逾時，煩請重新選擇取餐時間"
  },
  restaurantPreorderTimeSelector: {
    label: "請選擇取餐時間",
    date: "{today}（今日）取餐",
    desc:
      "* 為維持餐點品質為最新鮮狀態，以及餐點保存空間的乾淨衛生，請於預定取餐時間20分鐘內前往取餐。",
    time: "取餐時間",
    order: "請選擇外帶餐點",
    closed: "目前無法預訂",
    unavailable: "11:00 以後開放當日預訂"
  },
  restaurantDeliveryTimeSelector: {
    label: "請選擇送達時間",
    date: "{today}（今日）取餐",
    desc: "@:deliveryCartPanel.desc",
    time: "送達時間",
    order: "請選擇外送餐點",
    closed: "目前無法預訂",
    unavailable: "11:00 以後開放當日預訂"
  },
  preorderRecord: {
    pickupTime: "取餐時間",
    status: "訂單狀態",
    number: "訂單編號",
    pickupButton: "我要取餐",
    pickupDesc1:
      "餐點未準備完成，尚無法點選「我要取餐」；餐廳準備好餐點後，會再另寄簡訊通知您。",
    pickupDesc2:
      "為維持餐點品質為最新鮮狀態，以及餐點保存空間的乾淨衛生，請於預定取餐時間20分鐘內前往取餐。",
    orderDetail: "餐點明細",
    productNote: "備註：",
    total: "小計 {count} 項",
    shippingFee: "運費",
    points: "新光三越貴賓卡點數折抵",
    totalAmount: "總金額",
    note: "訂單備註",
    createdTime: "外帶時間",
    customerName: "訂購人姓名",
    customerPhone: "訂購人電話",
    customerEmail: "訂購人信箱",
    invoiceNumber: "發票號碼",
    invoiceDonation: "發票是否捐贈",
    invoiceAddress: "發票中獎寄送地址",
    companyName: "抬頭",
    taxIdNumber: "統一編號",
    cancel: "取消訂單",
    contact: "聯絡客服",
    back: "返回外帶外送紀錄",
    desc:
      "餐點準備完成後，將會寄發簡訊通知您。餐廳接單後，無法自行取消訂單，如有特殊需求，請聯絡客服。",
    deliveryAddress: "外送地址"
  },
  skmpay: {
    step: {
      1: "訂單資訊",
      2: "付款",
      3: "完成訂購"
    },
    success: "訂購完成",
    fail: "訂單交易失敗",
    cancel: "訂單交易取消",
    backToFoodMap: "回美食地圖",
    checkoutOrder: "檢視完整訂單",
    resultNote:
      "餐點準備完成後，會再另外寄簡訊通知您。如需更改或取消訂單，請至檢視完整訂單頁面點選「聯絡客服」。",
    paymentDetail: "購物明細",
    deliveryTime: "預計送餐時間",
    pickupTime: "預計取餐時間",
    mealDeliverAddress: "送餐地址"
  },
  deliveryRecord: {
    pickupTime: "外送時間",
    createdTime: "外送時間"
  },
  preorderGroup: {
    formatedPickupTimeToday: "M/D (今日)",
    formatedPickupTime: "M/D (dd)",
    pickDate: "選擇日期",
    pickTime: "選擇時段",
    estimatedPickupTime: "預計取餐時間",
    warningTag: "提醒",
    warningContent:
      "請選擇外帶日期，取餐時間為您所預訂日期12:00-20:00 部份餐廳午休時間不開放外帶。"
  },
  errorCodes: {
    100000: "資料庫錯誤",
    100001: "資料庫同步錯誤",
    100002: "料庫資料錯誤",
    200001: "擷取訂位上限錯誤",
    300001: "您已經候位本餐廳尚未入座或取消，請完成後再次候位。",
    300002: "訂位時段已滿",
    400001: "查無訂位數量",
    500000: "",
    500001: "無效電話號碼",
    500002: "訂位人數超過許可上限",
    "skm-03": "美食地圖系統異常，暫時無法登入。",
    "skm-30": "美食地圖系統異常，暫時無法登入。",
    "skm-63": "美食地圖系統異常，暫時無法登入。",
    "skm-96": "會員系統異常，暫時無法登入，請稍後再試。",
    "skm-N1": "因登入已逾時，請重新登入。",
    "skm-N8": "您的會員卡皆已失效。",
    "skm-12": "登入狀態異常，請重新登入。",
    "skm-25": "登入狀態異常，請重新登入。",
    "skm-54": "登入狀態異常，請重新登入。"
  },
  preorderTutorial: {
    takeoutSteps: "外帶外送五步驟",
    pageTitle: "外帶外送說明",
    breadcrumb: "外帶外送說明",
    entranceText: "外帶外送說明",
    needHelps: "需要幫助？",
    needHelp: "需要幫助嗎？",
    details: "詳細說明",
    faq: "常見問題",
    button: {
      step1: "登入會員",
      step2: "選擇「您要的服務」",
      step3: "選擇餐廳與餐點",
      step4: "線上結帳",
      step5: "完成取餐享用餐點"
    },
    detailItem: {
      login: {
        header: "登入會員",
        step1: {
          title: "註冊與登入",
          text:
            '使用預訂外帶/外送服務必須進行會員登入。請點選右上角的<span class="anchor" hash="{signIn}">貴賓卡會員登入</span>，請直接輸入帳號密碼登入。<br />尚未註冊過的貴賓請點選<a class="anchor" target="_blank" href="{signUp}">加入美食地圖會員</a>並填寫個人資料與詳閱會員權益及使用約定條款後，即可享有新光三越美食地圖服務。'
        }
      },
      selectBranch: {
        header: "輸入查詢的店或輸入地址",
        step1: {
          title: "",
          text:
            '於<a class="anchor" href="{home}">首頁</a>或分店頁內選擇「您想前往的分店」或於「查詢符合美食外送的分店」輸入您的地址查詢。'
        }
      },
      selectRestaurant: {
        header: "選擇餐廳與餐點",
        service: "服務項目",
        step1: {
          title: "選擇餐廳與服務",
          text:
            "於分店頁選擇餐廳與服務，若選擇外送服務，請再次確認您的外送地址。"
        },
        step2: {
          title: "點餐",
          text:
            "點擊餐點可選擇餐點細節或註明餐點特殊需求，選擇份數後新增至購物車。"
        },
        step3: {
          title: "查看購物車",
          text:
            "將餐點放入購物車後，可由右下角購物車清單按鈕查看外帶細項，確認預訂內容。"
        },
        step4: {
          title: "購物車結帳",
          text: "選擇時間並確認訂單無誤，即可點擊下方按鈕前往結帳。"
        }
      },
      checkout: {
        header: "結帳",
        step1: {
          title: "選擇付款方式",
          text:
            "選擇付款方式，目前提供skm pay與信用卡線上刷卡全額付清服務，更多詳情請查看關於付款方式。"
        },
        step2: {
          title: "填寫訂購人與發票資訊",
          text:
            '填寫訂購人姓名、電話、Email 以及發票載具。<br>訂購人資料皆預設為您申辦會員時之資料，如當次訂購修改訂購人資料，將不影響會員資料，若有需調整會員資料，煩請至<a class="anchor" target="_blank" href="{joinMember}">新光三越官網貴賓卡會員中心</a>或skm app會員專區進行修改。'
        },
        step3: {
          title: "付款",
          text:
            "若選擇skm pay，系統將引導您開啟skm app進行付款。<br>若選擇信用卡，請輸入您的信用卡號並完成付款，付款成功後訂單將會送至餐廳，餐廳將於您預訂時間前開始為您準備餐點。為保障您可準時取餐，請儘速完成付款。"
        },
        step4: {
          title: "訂購完成",
          text:
            "訂單完成後，可點選前往檢視完整訂單，同時系統會寄發一封已完成訂購通知函與電子發票通知信到您的聯絡用E-mail信箱。<br />餐點準備完成後，會再另外寄簡訊通知您，如需更改或取消訂單，請至檢視完整訂單頁面店點選「聯絡客服」。"
        }
      },
      takeMeal: {
        header: "查詢訂單與取餐",
        step1: {
          title: "查詢訂單記錄",
          text:
            "點選右上方會員資訊，點選外帶外送紀錄可前往查看您所有的過往訂單。"
        },
        step2: {
          title: "外帶外送紀錄",
          text:
            "可以在此查看您所有的新進訂單與歷史紀錄。<br />確認訂單狀態與取餐時間或是檢視完整訂單，您也可以在這裡取消訂單。"
        },
        step3: {
          title: "取餐",
          text:
            "外帶-當外帶訂單狀態顯示「可前來取餐」即可前往餐廳，出示訂單記錄點選「我要取餐」，請出示 QR code 頁面給餐廳人員掃描來確認取餐，或者出示訂單編號來提供核銷。<br>外送-當外送訂單狀態顯示外送中，即代表外送員已出發至您的所在地，取餐時，請提供您的訂單編號，以供外送員核對，並請您簽名完成本次取餐。<br><br>*為響應環保，建議您可自備購物袋與餐具，一起愛護我們的地球。"
        }
      }
    },
    faqItem: {
      terms: {
        header: "條款宣告與平台權利",
        note1: {
          title: "條款宣告",
          text: `本服務條款（下稱「本條款」）約定您對新光三越百貨股份有限公司（下稱為「本公司」）所提供新光三越的美食地圖skm eats（以下統稱為「本平台」）的功能。使用本平台前請詳細閱讀本條款及本公司不定期調整之內容，如您開始使用任何功能，即表示您同意及接受本條款及修改內容。如果您不同意或不願遵守本條款，請勿使用本平台之任何功能。
          <br>本平台僅供年滿 20 歲的自然人和法人使用。未滿 20 歲的者，須先徵得法定代理人的同意，且法定代理人同意接受本條款，並對您的行為承擔全部責任，且全額支付您使用本平台購買餐點產生的任何費用。若您尚未徵得父母或法定代理人的同意，請勿使用本平台之任何功能。若您已使用，則視為已徵得父母或法定代理人的同意。
          <br>如本條款中的任何規定被法律認定為無效、違法或不可執行，則該類規定不影響本條款其餘規定，其餘規定應視為有效。本公司本條款、訂單及新光三越APP中之條款，構成了您與本公司之間的完整協議。
          <br>本條款依中華民國法律為準據法。如有爭議，以臺灣臺北地方法院為第一審管轄法院。
          <br>非餐點類之外送亦適用本條款相關付款及運送規範。
          <br>本公司將會依據本公司新光三越隱私權政策處理您的個人資料，該隱私政策構成本條款的一部分。您同意本公司及其他相關連的第三方，根據本條款與相關隱私政策中進一步詳細說明的規定，收集、使用、處理您的個人資料。`
        },
        note2: {
          title: "平台權利",
          text: `
          您同意本平台上的內容是在「既有」和「現有」的基礎上提供，您自行決定使用本平台以及透過本平台獲得的任何內容、商品、產品或服務，由您自行承擔相應風險。本公司會盡力確保可以提供本平台以及提供的服務，但本公司不擔保亦不聲明在提供服務時，始終安全、即時、不間斷、無錯誤、無技術困難、缺陷或系統中毒。且本平台可能會因定期或日常維護，以及因網路、電子通訊或不可抗力事件造成的停止運作，而暫時中斷。
          <br>本公司不承擔因本公司的網頁而造成的任何延誤、故障、錯誤、遺漏或丟失傳送的資料，轉發的病毒或其他汙染，破壞屬性的任何責任。
          <br>本公司不為任何店家的延誤、疏忽，與因不可抗力因素影響造成的延遲或疏忽負責。您於本平台上執行之操作均由系統即時接收，並依相關服務指令即時或批次進行處理，除本公司另有明定外，交易經確認後，即不得撤回、取消或修改。
          <br>本公司已採取了可以合理防止網路詐騙的方式，並確保您私人資料的安全。但不因重大事件﹝如因電腦伺服器的受損或破壞、第三方的違約﹞而負責。
          <br>一旦發現有濫用優惠券或其他有詐騙之虞時，本公司可以立即停止並拒絕您使用本平台，且得向您請求賠償。
          <br>您同意如因您不當使用本平台或您違反本條款而致的任何第三方賠償責任，並不會向本公司任何求償。

          `
        },
        note3: {
          title: "第三方連結、網站與法律",
          text: `
          本平台可能包含其他第三方網站的連結，點按這些連結，即視為您同意自行承擔轉至這些網站的風險。
          <br>本公司對於第三方網站或連結之內容不負任何責任。如您因使用這些第三方連結和網站，而受有損害，本公司並不對此負任何責任。
          <br>請勿在本公司本平台上使用任何自動化或外掛程式來獲取本平台的程式。
          <br>請勿從本公司本平台收集或尋求任何可識別的個人資訊、使用本平台提供的系統做任何商業招攬之目的之事項、以任何理由招攬本平台的使用者、未經本公司同意自行發佈或發放任何折價卷或網站連接的代碼、破壞或破解本平台。
          <br>如果本公司合理懷疑或發現您使用本台平有欺詐行為、違反善良風俗或任何違反條款的行為，本公司有權取消您的任何訂單與服務或暫停停用、刪除或終止您的帳戶和存取平台的權限。

          `
        }
      },
      meal: {
        header: "關於餐點",
        note1: {
          title: "過敏原及食用期限說明",
          text: `
          本公司不保證店家出售的商品不含特定過敏原。店家在準備特定餐點時，可能會使用對您有過敏之食材。若您對特定食物過敏，請在訂購前先行了解，並於訂購時於訂單中特別標註或於訂購前先與店家聯繫。
          <br>在領取餐點後，請於店家所建議的最佳食用時間或盡速將餐點食用完畢，以確保餐點品質。
          `
        },
        note2: {
          title: "確認餐點",
          text: `當外送員將您所訂購之餐點交付給您時，請先檢查餐點之外觀、品項、數量等是否正確，如有錯誤請立即透過電話或來信通知本公司。`
        },
        note3: {
          text:
            "若須登打公司統一編號，請務必於結帳時點選【電子發票證明聯紙本】，並輸入公司統一編號及公司名稱，前往您購買的店別之卡友服務中心出示您該筆訂單的資訊索取紙本發票。"
        }
      },
      order: {
        header: "訂單相關",
        note1: {
          title: "訂單的成立與支付",
          text: `
          您透過本平台下單後，本公司會向您發送一封包含訂單資訊的電子郵件，以確認您的訂單，電子郵件內容包括餐點費用、運送費用及相關稅額（如有）。店家於接受您的訂單後開始準備餐點，您於訂購時須載明運送地點與聯繫方式。
          <br>餐點及運送費用支付方式可使用信用卡與skm pay全額付清，並可使用國民旅遊卡消費及補助核銷。本公司接受之信用卡包括VISA、MasterCard、JCB等三家國際發卡組織之信用卡。請於使用時務必確實填寫信用卡資料；若發現有不實登錄或任何未經持卡人許可而盜刷其信用卡的情形時，相關責任由您承擔。本公司有權暫停或終止您的權益，並行使相關權利。
          <br>信用卡線上交易授權必須透過本公司與您的發卡銀行聯繫，倘若發生任何無法完成線上交易授權的原因，相關的訊息由於牽涉個人資料，建議您可自行聯繫發卡銀行了解原因。
          <br>如出現無法正常結帳情形，可能係因：
          <br>(1)網路不穩、同時間訂購人數較多，建議您稍等一段時間再重新訂購商品。
          <br>(2)信用卡刷卡額度不足造成之刷卡失敗，由於本平台交易皆為線上進行，為確保持卡人的資料安全，請您親自向發卡銀行確認。
          <br>【※如您使用非本人信用卡所產生爭議，由您自行承擔，如造成本公司受有損害，並應負損害賠償責任。】
          `
        },
        note2: {
          title: "費用",
          text: `
          本平台所有價格均以新台幣計算，並皆已含稅。本公司在可能在您流覽餐點時調整餐點及遞送費用，但此調整不會影響已確認之訂單。如餐點價格有錯誤，本公司將會立即通知您，您得選擇是否以正確價格訂購，或不需支付任何費用取消訂單，並全額退款。
          <br>您訂單應支付的全部價格將於本平台的結帳頁面顯示，包含餐點費用、運送費用與稅額（如有）。
          <br>本公司店家本平台響應電子發票政策，全面採用雲端發票，電子發票證明聯不再主動寄出。本平台消費所開立之「雲端發票」將於發票開立後48小時內上傳至財政部電子發票整合服務平台留存，您可於該平台查詢發票資訊，亦可於本網站訂單查詢頁面查詢發票號碼與交易明細。
          <br>本平台將每期自動幫您兌獎雲端發票，若您幸運中獎將以E-mail方式或簡訊寄發中獎通知，更多訊息，請至新光三越電子發票使用說明(https://www.skm.com.tw/w/sk?a=ilanztu)了解。若您須登打公司統一編號，請務必於結帳時點選【電子發票證明聯紙本】，並輸入公司統一編號及公司名稱，前往您購買的店別之卡友服務中心或店家出示您該筆訂單的資訊索取紙本發票。
          `
        },
        note3: {
          title: "資料正確性(收件人、連絡方式、地點、時間等",
          text: `
          您透過本平台下單，請在點按「結帳」後依指示進行操作，您同意並確認您提供的所有資訊（包括金額、運送地點、連繫方式、付款資訊和優惠碼（如有））均屬正確，同時保證您於訂購時所提供的信用卡為您本人所有或已獲得信用卡持卡人同意使用【※如您使用非本人信用卡所產生爭議，由您自行承擔，如造成本公司受有損害，並應負損害賠償責任。】，如果您收到包含您訂單資訊的確認電子郵件，即表示您已成功下單。店家將負責準備您的餐點，並交由本公司委託對象運送。若因餐點內容錯誤、傾覆等需要求退款，請您撥打新光三越免付費客服專線。若須退款時，您亦保證您所提供之帳戶資訊為您本人所有且正確無誤。
          <br>餐點將會運送到您於訂單中填寫的運送地點，您必須確保您所填寫的地址正確無誤，並且有人收受餐點，如果您所填寫之地址不正確，本公司將無法運送餐點給您，您仍需承擔此訂單之費用。本公司不負責審查運送地點領取餐點之人（包括但不限於訂購之本人、其親友、管理員或其他人），是否為您本人或者是否有權收受餐點。餐點提供予領取人時，所有風險與責任將同時轉移給您。
          <br>本公司在您提交訂單後，如果要更改運送地點，您必須提前與本公司聯繫。如本公司無法更改運送地點，您可選擇取消訂單，但如店家已接受訂單，您仍須支付餐點費用，且如外送員已至店家領取餐點，您亦須支付運送費用。
          <br>本公司如因下列原因造成餐點運送失敗，您仍須支付餐點及運送費用：
          <br>(1)無人領取餐點。
          <br>(2)外送員以您提供之聯絡資訊聯繫後，仍無法與您取得聯繫。
          <br>(3)無適當的途徑交付餐點。
          <br>(4)缺乏合適地點來放置餐點。
          `
        }
      },
      takeMeal: {
        header: "關於取餐",
        note1: {
          title: "外帶自取",
          text: `
          您可以選擇直接從店家外帶自取（下稱「自取」），並可至【預訂外帶紀錄】確認訂單狀態，在店家確認以前，您可自行點選【取消訂單】申請訂單取消。若無法線上成功取消訂單，表示店家已確認您的餐點，則無法取消您的訂單。若您是有預定取餐時間，在未取餐時如希望取消訂單，請您於預訂取餐時間至少一小時前選取【聯絡客服】通知本公司，將由客服人員為您查詢處理。您應確認電子郵件內註明您自行取貨的時間（下稱「取貨時間」）。店家會在取貨時間之前準備訂單。惟仍可能發生合理的延遲。
          <br>如果由於您的個人因素而造成延遲自取，您應承擔任何餐點毀損或餐點品質變化等風險。於此情況下，您無權要求換貨或退貨。在自取時自行檢查餐點，如您已離開店家，餐點之任何問題，本公司或店家並不負任何責任。
          <br>當確認完成取消訂單後，系統將發出訂單取消通知信，此訂單款項將於7~10個工作天內進行退款作業，退貨款項將退至您原付款之信用卡帳戶。實際入帳日期將視您的發卡銀行作業時間而定。
          `
        },
        note2: {
          title: "送餐地點、時間的彈性",
          text: `
          本平台所提供的交貨時間僅為預估時間，餐點之實際運送時間可能會因當時之天候與交通狀況等不可抗力之事由或店家之準備餐點進度而出現差異。您不得因餐點遲延而要求取消訂單及退款，或要求本公司或店家賠償或補償任何費用。
          <br>本平台所有的訂單將由合作單位負責指派外送員運送。本公司會盡力要求合作單位於交貨時間內送達餐點本公司。
          <br>如因不可抗力，致外送員無法將餐點運送到您填寫之運送地址時，本公司將以您提供之聯絡方式通知您，並提供您取消訂單或是改變外送地址等服務。
          <br>本公司在本平台上輸入您的運送地點，您就能知悉得向您提供服務的店家。惟可能因天氣和交通條件等不可抗力因素，服務範圍可能會變動。
          `
        },
        note3: {
          title: "收件人與代收人",
          text: `
          當外送員將餐點送至您填寫之運送地點後，將通知您取餐，基本上本公司會於運送地點等待十分鐘，惟如仍無人前來領取餐點或無法聯繫到您時，外送員將視情況將餐點放置在合適地點，但此非外送員之義務，您不得以外送員未留置而要求賠償。若於時間內皆無獲得您回應且無合適地點放置餐點，外送員將離開運送地點，並視同您放棄該餐點，本公司無需退款或採取其他補救措施。
          `
        }
      },
      cancelOrder: {
        header: "取消訂單與退貨",
        note1: {
          title: "訂單的取消",
          text: `
          您可至【預訂外帶/外送記錄】確認訂單狀態，在店家尚未接單之前，您可自行點選【取消訂單】申請訂單取消，並獲取訂單的退款。若無法線上成功取消訂單，表示店家已確認接單、餐點準備中或外送員已領取餐點，在這之後如果您仍決定取消訂單，即表示您瞭解店家不會向您提供退款，本公司也不會運送已取消訂單的餐點。若您是有預定取餐時間，在未取餐時希望取消訂單，請您於預訂取餐時間至少一小時前選取【聯絡客服】通知本公司，將由客服人員為您查詢處理。
          <br>當因任何原因導致店家不供應您所訂購之餐點或產品或外送員無法送餐或您因其他不可抗力原因而欲取消您的訂單，且本公司亦取得店家同意時，本公司將會視情況取消訂單，並退還您所支付之費用，其中包括運費或其他服務費用，但不包含訂單所使用的優惠或折價券。
          <br>當需退款時，此款項將於7~10個工作天內進行退款作業，退貨款項將退至您原付款之信用卡帳戶。（實際入帳日期將視您的發卡銀行作業時間而定）
          `
        },
        note2: {
          title: "訂單錯誤、商品缺失、商品缺陷",
          text: `
          收到訂單餐點或商品後，如果您發現有問題時（如：內容錯誤或餐點毀損等），請立即聯繫客服。本公司可能會要求您拍照或提供其他資訊，以查明店家或外送員是否提供錯誤的餐點或餐點毀損情形，您可以拒絕接受此餐點，本公司將會全額退還餐點費用。但如果是您有特殊餐點備註，本公司和店家將盡力配合，然如果您的特殊備註是不可行或不合理而未完全符合您的需求時，店家得依標準程序為您製作餐點，此情形將不屬於退款範疇。
          `
        },
        note3: {
          title: "無法退貨原則之聲明",
          text: `
          本平台提供之餐點因屬於易於腐敗、保存期限較短或退貨時即將逾期之商品（如現做的餐點食物、蛋糕、麵包、生鮮食品等），除本條款另有載明之外，恕無法接受無條件退貨之要求。
          `
        }
      },
      other: {
        header: "其他",
        note1: {
          title: "優惠券",
          text: `本公司可能會不定時舉辦優惠、折扣等行銷和促銷活動。優惠券具有使用期限，使用次數以優惠券表示為準。優惠券不可與其他促銷活動、折扣或其他優惠一併使用。優惠券須遵守相關條件。除另有說明外，優惠券只能在本平台上使用。優惠券不能兌換成現金。本公司保留取消、停止或拒收任何優惠券的權利，恕不另行通知。店家本公司隨時新增或禁止某些店家使用優惠券，而不必事先通知您。
          `
        }
      }
    }
  },
  realMap: {
    expandMap: "顯示完整地圖",
    collapseMap: "收合地圖",
    dragHint: "拖曳以查看地圖"
  },
  remind: "提醒"
};
