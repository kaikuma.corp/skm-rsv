import zhTW from "./zh-TW";
import enUS from "./en-US";

const messages = {
  "zh-TW": zhTW,
  "en-US": enUS
};

export default messages;
