// moved from store/group.js
// let utils.js have no dependency on store/group.js.
// if so, utils.js will have final dependency on i18n.js
// that will result in failed unit test with "Cannot read property 'state' of undefined".

export default {
  waiting: null,
  bookingNoon: { from: "9:00", to: "14:00" },
  bookingAfternoon: { from: "14:00", to: "17:00" },
  bookingNight: { from: "17:00", to: "00:45" }
};
