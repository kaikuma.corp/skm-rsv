export default {
  waiting: null,
  bookingNoon: { from: "9:00", to: "14:00" },
  bookingAfternoon: { from: "14:00", to: "17:00" },
  bookingNight: { from: "17:00", to: "00:45" }
};
