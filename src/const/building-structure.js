import i18n from "../i18n";
/**
 * bg doc (as far as i know)
 * 001: 餐廳
 * 002: 女性服飾
 * 003: 化妝品
 * 006: 家庭用品
 * 007: 男女綜合服飾
 * 008: 童裝 玩具 婦嬰用品
 * 009: 電影院
 */

// NOTE: { wall, top, main } should be modified.
const taichung = [
  {
    id: "201",
    name: i18n.t("building.skm-taichung.main"),
    top: "taichung",
    parkingFloors: [{}, {}, {}, {}],
    basementFloors: [
      { num: -1, wall: "basement", bg: "002" },
      { num: -2, wall: "basement", bg: "001" }
    ],
    groundFloors: [
      { num: 1, wall: "chung-1", bg: "003" },
      { num: 2, wall: "chung-2", bg: "003" },
      { num: 3, wall: "chung-3", bg: "007" },
      { num: 4, wall: "chung-4", bg: "002" },
      { num: 5, wall: "chung-5", bg: "007" },
      { num: 6, wall: "chung-6", bg: "008" },
      { num: 7, wall: "chung-7", bg: "006" },
      { num: 8, wall: "chung-8", bg: "006" },
      { num: 9, wall: "chung-9", bg: "001" },
      { num: 10, wall: "chung-10", bg: "001" },
      { num: 11, wall: "chung-11", bg: "002" },
      { num: 12, wall: "chung-12", bg: "001" },
      { num: 13, wall: "chung-13", bg: "009" },
      { num: 14, wall: "chung-14", bg: "009" }
    ]
  }
];

const nanxi = [
  {
    id: "101",
    name: i18n.t("building.skm-nanxi.building101"),
    top: "nanxi-101",
    parkingFloors: [{}, {}],
    basementFloors: [
      { num: -1, wall: "basement", bg: "002" },
      { num: -2, wall: "basement", bg: "001" }
    ],
    groundFloors: [
      { num: 1, wall: "nanxi-101-1", bg: "003" },
      { num: 2, wall: "nanxi-101-2", bg: "002" },
      { num: 3, wall: "nanxi-101-2", bg: "002" },
      { num: 4, wall: "nanxi-101-2", bg: "002" },
      { num: 5, wall: "nanxi-101-5", bg: "007" },
      { num: 6, wall: "nanxi-101-6", bg: "008" },
      { num: 7, wall: "nanxi-101-7", bg: "004" },
      { num: 8, wall: "nanxi-101-8", bg: "007" },
      { num: 9, wall: "nanxi-101-8", bg: "010" }
    ]
  },
  {
    id: "103",
    name: i18n.t("building.skm-nanxi.building103"),
    top: "nanxi-103",
    parkingFloors: [{}, {}],
    basementFloors: [{ num: -1, wall: "basement", bg: "007" }],
    groundFloors: [
      { num: 1, wall: "nanxi-103-1", bg: "007" },
      { num: 2, wall: "nanxi-103-2", bg: "002" },
      { num: 3, wall: "nanxi-103-2", bg: "002" },
      { num: 4, wall: "nanxi-103-2", bg: "007" },
      { num: 5, wall: "nanxi-103-5", bg: "011" },
      { num: 6, wall: "nanxi-103-6", bg: "011" },
      { num: 7, wall: "nanxi-103-7", bg: "001" },
      { num: 8, wall: "nanxi-103-8", bg: "001" }
    ]
  }
];

const tainan = [
  {
    id: "321",
    name: i18n.t("building.skm-tainanplace.building321"),
    top: "tainan-321",
    parkingFloors: [{}, {}, {}, {}],
    basementFloors: [
      { num: -1, wall: "basement", bg: "006" },
      { num: -2, wall: "basement", bg: "001" }
    ],
    groundFloors: [
      { num: 0, displayNum: "G", wall: "tainan-321-1", bg: "003" },
      { num: 1, displayNum: 1, wall: "tainan-321-2", bg: "002" },
      { num: 2, displayNum: 2, wall: "tainan-321-2", bg: "007" },
      { num: 3, displayNum: 3, wall: "tainan-321-2", bg: "008" },
      { num: 4, displayNum: 4, wall: "tainan-321-2", bg: "002" },
      { num: 5, displayNum: 5, wall: "tainan-321-2", bg: "007" },
      { num: 6, displayNum: 6, wall: "tainan-321-2", bg: "001" },
      { num: 7, displayNum: 7, wall: "tainan-321-2", bg: "009" },
      { num: 8, displayNum: 8, wall: "tainan-321-2", bg: "009" },
      { num: 9, displayNum: 9, wall: "tainan-321-2", bg: "009" }
    ]
  },
  {
    id: "322",
    name: i18n.t("building.skm-tainanplace.building322"),
    top: "tainan-322",
    parkingFloors: [{}],
    basementFloors: [
      { num: -1, wall: "basement", bg: "007" },
      { num: -2, wall: "basement", bg: "001" },
      { num: -3, wall: "basement", bg: "001" }
    ],
    groundFloors: [{ num: 1, wall: "tainan-322-1", bg: "006" }]
  }
];

const TaipeiStation = [
  {
    id: "111",
    name: i18n.t("building.skm-TaipeiStation.building111"),
    top: "TaipeiStation-111",
    parkingFloors: [],
    basementFloors: [
      { num: -1, wall: "basement", bg: "001" },
      { num: -2, wall: "basement", bg: "001" }
    ],
    groundFloors: [
      { num: 1, displayNum: 1, wall: "TaipeiStation-111-1", bg: "003" },
      { num: 2, displayNum: 2, wall: "TaipeiStation-111-2", bg: "007" },
      { num: 3, displayNum: 3, wall: "TaipeiStation-111-3", bg: "007" },
      { num: 4, displayNum: 4, wall: "TaipeiStation-111-4", bg: "002" },
      { num: 5, displayNum: 5, wall: "TaipeiStation-111-5", bg: "002" },
      { num: 6, displayNum: 6, wall: "TaipeiStation-111-6", bg: "007" },
      { num: 7, displayNum: 7, wall: "TaipeiStation-111-6", bg: "007" },
      { num: 8, displayNum: 8, wall: "TaipeiStation-111-6", bg: "008" },
      { num: 9, displayNum: 9, wall: "TaipeiStation-111-6", bg: "007" },
      { num: 10, displayNum: 10, wall: "TaipeiStation-111-6", bg: "006" },
      { num: 11, displayNum: 11, wall: "TaipeiStation-111-6", bg: "011" },
      { num: 12, displayNum: 12, wall: "TaipeiStation-111-6", bg: "001" },
      { num: 13, displayNum: 13, wall: "TaipeiStation-111-6", bg: "001" }
    ]
  }
];

const TaipeiTianmu = [
  {
    id: "151",
    name: i18n.t("building.skm-TaipeiTianmu.building151"),
    top: "TaipeiTianmu-151",
    parkingFloors: [{}, {}, {}],
    basementFloors: [{ num: -1, wall: "basement", bg: "001" }],
    groundFloors: [
      { num: 1, displayNum: 1, wall: "TaipeiTianmu-151-1", bg: "003" },
      { num: 2, displayNum: 2, wall: "TaipeiTianmu-151-2", bg: "002" },
      { num: 3, displayNum: 3, wall: "TaipeiTianmu-151-3", bg: "002" },
      { num: 4, displayNum: 4, wall: "TaipeiTianmu-151-4", bg: "006" },
      { num: 5, displayNum: 5, wall: "TaipeiTianmu-151-5", bg: "007" },
      { num: 6, displayNum: 6, wall: "TaipeiTianmu-151-6", bg: "008" },
      { num: 7, displayNum: 7, wall: "TaipeiTianmu-151-7", bg: "001" }
    ]
  },
  {
    id: "152",
    name: i18n.t("building.skm-TaipeiTianmu.building152"),
    top: "TaipeiTianmu-152",
    parkingFloors: [{}, {}, {}],
    basementFloors: [{ num: -1, wall: "basement", bg: "009" }],
    groundFloors: [
      { num: 1, displayNum: 1, wall: "TaipeiTianmu-152-3", bg: "003" },
      { num: 2, displayNum: 2, wall: "TaipeiTianmu-152-4", bg: "002" },
      { num: 3, displayNum: 3, wall: "TaipeiTianmu-152-5", bg: "001" },
      { num: 4, displayNum: 4, wall: "TaipeiTianmu-152-6", bg: "009" }
    ]
  }
];

const KaohsiungZuoying = [
  {
    id: "351",
    name: i18n.t("building.skm-KaohsiungZuoying.building351"),
    top: "KaohsiungZuoying-351",
    parkingFloors: [{}, {}],
    basementFloors: [
      { num: 0, displayNum: "BM", wall: "basement", bg: "" },
      { num: -1, displayNum: "B1", wall: "basement", bg: "007" },
      { num: -2, displayNum: "B2", wall: "basement", bg: "001" }
    ],
    groundFloors: [
      { num: 1, displayNum: 1, wall: "KaohsiungZuoying-351-1", bg: "003" },
      { num: 2, displayNum: 2, wall: "KaohsiungZuoying-351-2", bg: "002" },
      { num: 3, displayNum: 3, wall: "KaohsiungZuoying-351-3", bg: "002" },
      { num: 4, displayNum: 4, wall: "KaohsiungZuoying-351-4", bg: "002" },
      { num: 5, displayNum: 5, wall: "KaohsiungZuoying-351-5", bg: "008" },
      { num: 6, displayNum: 6, wall: "KaohsiungZuoying-351-6", bg: "007" },
      { num: 7, displayNum: 7, wall: "KaohsiungZuoying-351-7", bg: "007" },
      { num: 8, displayNum: 8, wall: "KaohsiungZuoying-351-8", bg: "001" },
      { num: 9, displayNum: 9, wall: "KaohsiungZuoying-351-9", bg: "004" },
      { num: 10, displayNum: 10, wall: "KaohsiungZuoying-351-10", bg: "005" },
      { num: 11, displayNum: 11, wall: "KaohsiungZuoying-351-11", bg: "005" }
    ]
  },
  {
    id: "352",
    name: i18n.t("building.skm-KaohsiungZuoying.building352"),
    top: "KaohsiungZuoying-352",
    paddingBottom: 2,
    parkingFloorsTop: true,
    parkingFloors: [
      { num: 6, displayNum: "R", wall: "KaohsiungZuoying-352-6" },
      { num: 5, displayNum: 5, wall: "KaohsiungZuoying-352-5" }
    ],
    basementFloors: [
      { num: -1, displayNum: "B1", wall: "basement", bg: "", parking: true },
      { num: -2, displayNum: "B2", wall: "basement", bg: "001" }
    ],
    groundFloors: [
      { num: 1, displayNum: 1, wall: "KaohsiungZuoying-352-1", bg: "004" },
      { num: 2, displayNum: 2, wall: "KaohsiungZuoying-352-2", bg: "008" },
      { num: 3, displayNum: 3, wall: "KaohsiungZuoying-352-3", bg: "001" },
      { num: 4, displayNum: 4, wall: "KaohsiungZuoying-352-4", bg: "001" }
    ]
  }
];

const KaohsiungSanduo = [
  {
    id: "301",
    name: i18n.t("building.skm-KaohsiungSanduo.building301"),
    top: "KaohsiungSanduo-301",
    parkingFloors: [{}, {}, {}, {}],
    basementFloors: [
      { num: -1, wall: "basement", bg: "007" },
      { num: -2, wall: "basement", bg: "001" }
    ],
    groundFloors: [
      { num: 1, displayNum: 1, wall: "KaohsiungSanduo-301-1", bg: "003" },
      { num: 2, displayNum: 2, wall: "KaohsiungSanduo-301-2", bg: "003" },
      { num: 3, displayNum: 3, wall: "KaohsiungSanduo-301-3", bg: "002" },
      { num: 4, displayNum: 4, wall: "KaohsiungSanduo-301-3", bg: "002" },
      { num: 5, displayNum: 5, wall: "KaohsiungSanduo-301-3", bg: "002" },
      { num: 6, displayNum: 6, wall: "KaohsiungSanduo-301-3", bg: "007" },
      { num: 7, displayNum: 7, wall: "KaohsiungSanduo-301-3", bg: "006" },
      { num: 8, displayNum: 8, wall: "KaohsiungSanduo-301-3", bg: "008" },
      { num: 9, displayNum: 9, wall: "KaohsiungSanduo-301-3", bg: "006" },
      { num: 10, displayNum: 10, wall: "KaohsiungSanduo-301-3", bg: "006" },
      { num: 11, displayNum: 11, wall: "KaohsiungSanduo-301-3", bg: "001" },
      { num: 12, displayNum: 12, wall: "KaohsiungSanduo-301-3", bg: "001" },
      { num: 13, displayNum: 13, wall: "KaohsiungSanduo-301-3", bg: "005" },
      { num: 14, displayNum: 14, wall: "KaohsiungSanduo-301-3", bg: "005" }
    ]
  }
];

const TainanZhongshan = [
  {
    id: "311",
    name: i18n.t("building.skm-TainanZhongshan.building311"),
    top: "TainanZhongshan-311",
    parkingFloors: [{}, {}, {}],
    basementFloors: [
      { num: -1, wall: "basement", bg: "006" },
      { num: -2, wall: "basement", bg: "001" }
    ],
    groundFloors: [
      { num: 1, displayNum: 1, wall: "TainanZhongshan-311-1", bg: "003" },
      { num: 2, displayNum: 2, wall: "TainanZhongshan-311-2", bg: "002" },
      { num: 3, displayNum: 3, wall: "TainanZhongshan-311-4", bg: "002" },
      { num: 4, displayNum: 4, wall: "TainanZhongshan-311-3", bg: "002" },
      { num: 5, displayNum: 5, wall: "TainanZhongshan-311-3", bg: "002" },
      { num: 6, displayNum: 6, wall: "TainanZhongshan-311-3", bg: "006" },
      { num: 7, displayNum: 7, wall: "TainanZhongshan-311-3", bg: "007" },
      { num: 8, displayNum: 8, wall: "TainanZhongshan-311-3", bg: "008" },
      { num: 9, displayNum: 9, wall: "TainanZhongshan-311-3", bg: "004" },
      { num: 10, displayNum: 10, wall: "TainanZhongshan-311-3", bg: "011" },
      { num: 11, displayNum: 11, wall: "TainanZhongshan-311-3", bg: "011" },
      { num: 12, displayNum: 12, wall: "TainanZhongshan-311-3", bg: "001" },
      { num: 13, displayNum: 13, wall: "TainanZhongshan-311-5", bg: "005" }
    ]
  }
];

const TaoyuanStation = [
  {
    id: "171",
    name: i18n.t("building.skm-TaoyuanStation.building171"),
    top: "TaoyuanStation-171",
    parkingFloors: [{}, {}, {}, {}, {}],
    basementFloors: [{ num: -1, wall: "basement", bg: "001" }],
    groundFloors: [
      { num: 1, displayNum: 1, wall: "TaoyuanStation-171-1", bg: "003" },
      { num: 2, displayNum: 2, wall: "TaoyuanStation-171-2", bg: "002" },
      { num: 3, displayNum: 3, wall: "TaoyuanStation-171-3", bg: "002" },
      { num: 4, displayNum: 4, wall: "TaoyuanStation-171-3", bg: "002" },
      { num: 5, displayNum: 5, wall: "TaoyuanStation-171-3", bg: "002" },
      { num: 6, displayNum: 6, wall: "TaoyuanStation-171-3", bg: "007" },
      { num: 7, displayNum: 7, wall: "TaoyuanStation-171-3", bg: "007" },
      { num: 8, displayNum: 8, wall: "TaoyuanStation-171-3", bg: "008" },
      { num: 9, displayNum: 9, wall: "TaoyuanStation-171-3", bg: "004" },
      { num: 10, displayNum: 10, wall: "TaoyuanStation-171-3", bg: "001" },
      { num: 11, displayNum: 11, wall: "TaoyuanStation-171-3", bg: "001" }
    ]
  }
];

const TaoyuanDayou = [
  {
    id: "161",
    name: i18n.t("building.skm-TaoyuanDayou.building161"),
    top: "TaoyuanDayou-161",
    parkingFloors: [{}, {}],
    basementFloors: [
      { num: -1, wall: "basement", bg: "004" },
      { num: -2, wall: "basement", bg: "001" }
    ],
    groundFloors: [
      { num: 1, displayNum: 1, wall: "TaoyuanDayou-161-1", bg: "003" },
      { num: 2, displayNum: 2, wall: "TaoyuanDayou-161-2", bg: "002" },
      { num: 3, displayNum: 3, wall: "TaoyuanDayou-161-3", bg: "002" },
      { num: 4, displayNum: 4, wall: "TaoyuanDayou-161-4", bg: "007" },
      { num: 5, displayNum: 5, wall: "TaoyuanDayou-161-5", bg: "008" },
      { num: 6, displayNum: 6, wall: "TaoyuanDayou-161-6", bg: "011" },
      { num: 7, displayNum: 7, wall: "TaoyuanDayou-161-7", bg: "005" },
      { num: 8, displayNum: 8, wall: "TaoyuanDayou-161-8", bg: "001" },
      { num: 9, displayNum: 9, wall: "TaoyuanDayou-161-9", bg: "001" }
    ]
  }
];

const ChiayiChuiyang = [
  {
    id: "331",
    name: i18n.t("building.skm-ChiayiChuiyang.building331"),
    top: "ChiayiChuiyang-331",
    parkingFloors: [{}, {}, {}],
    basementFloors: [{ num: -1, wall: "basement", bg: "001" }],
    groundFloors: [
      { num: 1, displayNum: 1, wall: "ChiayiChuiyang-331-1", bg: "003" },
      { num: 2, displayNum: 2, wall: "ChiayiChuiyang-331-2", bg: "002" },
      { num: 3, displayNum: 3, wall: "ChiayiChuiyang-331-3", bg: "002" },
      { num: 4, displayNum: 4, wall: "ChiayiChuiyang-331-4", bg: "002" },
      { num: 5, displayNum: 5, wall: "ChiayiChuiyang-331-4", bg: "002" },
      { num: 6, displayNum: 6, wall: "ChiayiChuiyang-331-4", bg: "011" },
      { num: 7, displayNum: 7, wall: "ChiayiChuiyang-331-4", bg: "007" },
      { num: 8, displayNum: 8, wall: "ChiayiChuiyang-331-4", bg: "004" },
      { num: 9, displayNum: 9, wall: "ChiayiChuiyang-331-4", bg: "008" },
      { num: 10, displayNum: 10, wall: "ChiayiChuiyang-331-4", bg: "004" },
      { num: 11, displayNum: 11, wall: "ChiayiChuiyang-331-4", bg: "006" },
      { num: 12, displayNum: 12, wall: "ChiayiChuiyang-331-4", bg: "005" }
    ]
  }
];

export default {
  nanxifoodmap: {},
  "skm-taichung": taichung,
  "skm-nanxi": nanxi,
  "skm-tainanplace": tainan,
  "skm-TaipeiStation": TaipeiStation,
  "skm-TaipeiTianmu": TaipeiTianmu,
  "skm-KaohsiungZuoying": KaohsiungZuoying,
  "skm-KaohsiungSanduo": KaohsiungSanduo,
  "skm-TainanZhongshan": TainanZhongshan,
  "skm-TaoyuanStation": TaoyuanStation,
  "skm-TaoyuanDayou": TaoyuanDayou,
  "skm-ChiayiChuiyang": ChiayiChuiyang,

  "skm-xinyi": [
    {
      id: "124",
      name: "A4",
      top: "001",
      parkingFloors: [{}, {}, {}],
      basementFloors: [
        { num: -1, wall: "basement", bg: "001" },
        { num: -2, wall: "basement", bg: "006" }
      ],
      groundFloors: [
        { num: 1, wall: "001", bg: "003" },
        { num: 2, wall: "001", bridge: "002", bg: "002" },
        { num: 3, wall: "002", bg: "004" },
        { num: 4, wall: "002", bg: "003" },
        { num: 5, wall: "002", bg: "002" },
        { num: 6, wall: "002", bg: "001" }
      ]
    },
    {
      id: "122",
      name: "A8",
      top: "002",
      parkingFloors: [{}, {}, {}],
      basementFloors: [
        { num: -1, wall: "basement", bridge: "basement", bg: "001" },
        { num: -2, wall: "basement", bg: "001" }
      ],
      groundFloors: [
        { num: 1, wall: "005", bg: "003" },
        { num: 2, wall: "005", bridge: "003", bg: "002" },
        { num: 3, wall: "005", bridge: "004", bg: "006" },
        { num: 4, wall: "004", bg: "004" },
        { num: 5, wall: "003", bg: "002" },
        { num: 6, wall: "003", bg: "006" },
        { num: 7, wall: "003", bg: "004" }
      ]
    },
    {
      id: "123",
      name: "A9",
      top: "003",
      parkingFloors: [{}, {}, {}],
      basementFloors: [
        { num: -1, wall: "basement", bridge: "basement", bg: "006" },
        { num: -2, wall: "basement", bg: "001" }
      ],
      groundFloors: [
        { num: 1, wall: "006", bg: "003" },
        { num: 2, wall: "001", bridge: "005", bg: "002" },
        { num: 3, wall: "007", bg: "006" },
        { num: 4, wall: "006", bg: "002" },
        { num: 5, wall: "006", bg: "004" },
        { num: 6, wall: "006", bg: "001" },
        { num: 7, wall: "006", bg: "001" },
        { num: 8, wall: "006", bg: "001" },
        { num: 9, wall: "006", bg: "005" }
      ]
    },
    {
      id: "121",
      name: "A11",
      top: "004",
      parkingFloors: [{}, {}, {}],
      basementFloors: [
        { num: -1, wall: "basement", bg: "001" },
        { num: -2, wall: "basement", bg: "006" }
      ],
      groundFloors: [
        { num: 1, wall: "008", bg: "003" },
        { num: 2, wall: "008", bg: "002" },
        { num: 3, wall: "008", bg: "006" },
        { num: 4, wall: "008", bg: "004" },
        { num: 5, wall: "008", bg: "006" },
        { num: 6, wall: "008", bg: "005" }
      ]
    }
  ]
};
