const getBigMap = (groupId, buildingId, restaurantId) => {
  switch (groupId) {
    case "skm-xinyi":
      return "/static/images/building/xinyi/big.jpg";
    case "skm-taichung":
      return `/static/images/building/taichung/big.jpg`;
    case "skm-nanxi":
      return `/static/images/building/nanxi/${buildingId}-big.png`;
    case "skm-tainanplace":
      return "/static/images/building/tainanplace/big.png";
    case "skm-TaipeiTianmu":
      return "/static/images/building/TaipeiTianmu/all.jpg";
    case "skm-TaipeiStation":
      return "/static/images/building/TaipeiStation/111.jpg";
    case "skm-KaohsiungSanduo":
      return "/static/images/building/KaohsiungSanduo/big.jpg";
    case "skm-TainanZhongshan":
      return "/static/images/building/TainanZhongshan/big.jpg";
    case "skm-KaohsiungZuoying":
      return "/static/images/building/KaohsiungZuoying/big.jpg";
    case "skm-ChiayiChuiyang":
      return "/static/images/building/ChiayiChuiyang/big.jpg";
    case "skm-TaoyuanDayou":
      return "/static/images/building/TaoyuanDayou/big.jpg";
    case "skm-TaoyuanStation":
      return "/static/images/building/TaoyuanStation/big.jpg";
    default:
      throw new Error("Invalid group id");
  }
};

export default getBigMap;
