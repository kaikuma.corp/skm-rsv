import Vue from "vue";
var Rollbar = require("vue-rollbar");

Vue.use(Rollbar, {
  accessToken: "394c0d5707ff4531a413df9c4ffa58c8",
  captureUncaught: true,
  captureUnhandledRejections: true,
  enabled: process.env.VUE_APP_ENV === "development" ? false : true,
  environment: process.env.VUE_APP_ENV,
  payload: {
    client: {
      javascript: {
        code_version: "1.0",
        source_map_enabled: true,
        guess_uncaught_frames: true
      }
    }
  }
});

if (process.env.VUE_APP_ENV !== "development") {
  Vue.config.errorHandler = (err, vm, info) => {
    Vue.rollbar.error(err);
  };
}
