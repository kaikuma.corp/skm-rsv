module.exports = {
  plugins: [require("autoprefixer")],
  "postcss-pxtorem": {
    // 設計稿寬度的1/10,
    rootValue: 75,
    // 需要做轉化處理的屬性，如`hight`、`width`、`margin`等，`*`表示全部
    propList: ["*"]
  }
};
