# rsv

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Run your unit tests
```
yarn test:unit
```

### Run your end-to-end tests
```
yarn test:e2e
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Env
- development (local)
- lab (firebase)
- production (skm)


### Package
```
make package
```


## Notice
- 以上皆在無法實際串接api的情況下修改，會不會壞掉無法得知。
- 提供內容包含打包後檔案、原始檔，為了確認檔案與原始檔的一致性，恕不接受自行打包後檔案的驗收結果。
- 在沒有測試機的情況，請自行備份檔案，有還原的問題請自行解決。
