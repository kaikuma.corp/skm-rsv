const config = require("./config/config");
let plugins = [];

config.env.current === config.env.prod
  ? plugins.push("transform-remove-console")
  : void 0;

module.exports = {
  presets: [
    // "@vue/cli-plugin-babel/preset",
    [
      "@vue/app",
      {
        useBuiltIns: "entry"
      }
    ]
  ],
  plugins: [...plugins]
};
