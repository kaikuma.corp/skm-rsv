// var path = require("path");

module.exports = {
  publicPath: "/",
  css: {
    extract: process.env.NODE_ENV === "production",
    loaderOptions: {
      sass: {
        prependData: "@import '~stylesheets/_variables-and-mixins.scss';"
      }
    }
  },
  chainWebpack: webpackConfig => {
    webpackConfig.resolve.alias.set("stylesheets", "@/assets/stylesheets");
  },
  pwa: {
    name: "新光三越美食訂候位服務",
    themeColor: "#20232b",
    msTileColor: "#20232b",
    appleMobileWebAppCapable: "yes",
    appleMobileWebAppStatusBarStyle: "black",
    iconPaths: {
      favicon32: "static/favicon.ico",
      favicon16: "static/favicon.ico",
      appleTouchIcon: "img/icons/icon-152x152.png",
      // maskIcon: "img/icons/safari-pinned-tab.svg",
      msTileImage: "img/icons/icon-144x144.png"
    }
  },
  devServer: {
    host: "rsv.icoding.io",
    https: true
    //   proxy: {
    //     "^/vc/v/": {
    //       target: process.env.VUE_APP_SKM_API_NEW_ENDPOINT,
    //       ws: true,
    //       changeOrigin: true,
    //       pathRewrite: {}
    //     },
    //     "^/LoginPath/": {
    //       target: process.env.VUE_APP_SKM_API_NEW_ENDPOINT,
    //       ws: true,
    //       changeOrigin: true,
    //       pathRewrite: {}
    //     }
    //   }
  }
};
