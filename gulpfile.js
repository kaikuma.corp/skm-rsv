const { series, src, dest, watch } = require("gulp");
const mjml = require("gulp-mjml");
const pug = require("gulp-pug");
const rename = require("gulp-rename");
const clean = require("gulp-clean");
const data = require("gulp-data");
const gulpPlumber = require("gulp-plumber");
const fs = require("fs");

function cleanMjml() {
  return src("./mail-template/tmp", { read: false }).pipe(clean());
}

function makeMjml() {
  return src("./mail-template/*.pug")
    .pipe(gulpPlumber())
    .pipe(
      pug({
        pretty: true
      })
    )
    .pipe(
      rename({
        extname: ".mjml"
      })
    )
    .pipe(dest("./mail-template/tmp"))
    .pipe(mjml())
    .pipe(dest("./dist/mail"));
}

function watchMjml() {
  return watch("./mail-template/*.pug").on("change", () => {
    makeMjml();
  });
}

exports.buildMjml = series(makeMjml, cleanMjml);

exports.watchMjml = series(watchMjml);
