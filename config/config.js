// const args = require("minimist")(process.argv.slice(2));
// const path = require("path");
const config = {
  env: {
    current: process.env.VUE_APP_ENV,
    dev: "development",
    lab: "lab",
    prod: "production"
  }
};

module.exports = config;
